Please refer to Yuel 2 at <https://bitbucket.org/dokhlab/yuel2>

# Yuel: Compound-Protein Interaction Prediction with High Generalizability

Yuel is a structure-free compound-protein interaction predictor that predicts compound-protein interactions (CPI) with high generalizability. In contrast to other NNs, FC layers in Yuel are only applied to individual atom and residue features, and CPI is predicted by summing up the pairwise atom-residue interactions, each of which is evaluated by multiplying the feature of the corresponding compound atom and the feature of the corresponding protein residue.

We provide a website to use Yuel at: <http://yuel.dokhlab.org> or <https://dokhlab.med.psu.edu/cpi#YueL>

Please check out the latest version of Yuel at <https://bitbucket.org/dokhlab/yuel>

# Requirements:

- Python3
- pandas numpy matplotlib
- rdkit
- tensorflow==2.3.0 tensorflow-probability
- dm-sonnet graph_nets

We prepared a docker image that satisfies all these requirements.

    docker pull juw1179/yuel:0.0.1

# Usage:

    python yuel.py train [Option1] [Option2] ...
    python yuel.py predict [Option1] [Option2] ...

Options:

-  --data', '-d', type=str, default='data/pdbbind-train.tsv', help='input dataset'
-  --out', '-o', type=str, default='out.txt', help='output'
-  --batch_size', '-b', type=int, default=8, help='batch size'
-  --comp_size', '-c', type=int, default=64, help='max number of atoms in a compound'
-  --prot_size', '-p', type=int, default=1024, help='max number of residues in a protein'
-  --blosum62', '-s', type=str, default='data/blosum62.txt', help='path of blosum62 file'
-  --nfeat', '-e', type=int, default=32, help='number of features'
-  --num_conv_layers', type=int, default=3, help='num of convolution layers'
-  --num_graph_layers', type=int, default=3, help='num of graph layers'
-  --num_attention_layers', type=int, default=2, help='num of attention layers'
-  --epochs', type=int, default=50, help='num of epochs'
-  --model', type=str, default='models/davis.model', help='path of the saved model'
-  --cpt_path', type=str, default='checkpoints', help='checkpoint path'
-  --cpt_name', type=str, default='drift', help='checkpoint name'
-  --compound_column', type=str, default='Compound', help='compound column in the dataset file'
-  --compound_type_column', type=str, default='CompType', help='type of the compound: smi, inchi'
-  --protein_column', type=str, default='Protein', help='protein column in the dataset file'
-  --affinity_column', type=str, default='Affinity', help='affinity column in the dataset file'
-  --learning_rate', type=float, default=0.001, help='learning rate'
-  --gpu', type=int, default=-1, help='GPU'

# Data

We provided three datasets: PDBbind, Davis, and Metz. They are all in the './data' folder:

- 'pdbbind-train.csv': pdbbind training set
- 'pdbbind-test.csv': pdbbind test set
- 'davis-train.csv': davis training set
- 'davis-test.csv': davis test set
- 'metz-train.csv': metz training set
- 'metz-test.csv': metz test set

Additionally, we also shuffle the amino acids in pdbbind and davis datasets:

- 'pdbbind-test-shuffle.csv': amino acids in pdbbind is shuffled
- 'davis-test-shuffle.csv': amino acis in davis is shuffled

We also provide some result files in the './data' folder:

- 'pdbbind-pdbbind-out.csv': Train Yuel on pdbbind and then test it on pdbbind
- 'davis-davis-out.csv': Train Yuel on davis and then test it on davis
- 'davis-pdbbind-out.csv': Train Yuel on davis and then test it on pdbbind
- 'davis-metz-out.csv': Train Yuel on davis and then test it on metz

# Models

We provide two pre-trained models in the './models' folder:

- 'pdbbind.model': the model trained on pdbbind
- 'davis.model': the model trained on davis

# Training

- training on pdbbind

        python yuel.py train --data=data/pdbbind-train.csv \
                             --compound_column=Ligand \
                             --compound_type_column=CompType \
                             --protein_column=Sequence \
                             --affinity_column=Affinity \
                             --cpt_path=checkpoints/pdbbind \
                             --comp_size=64 \
                             --nfeat=32 \
                             --learning_rate=0.00005 \
                             --epochs=200 \
                             --prot_size=2048 \
                             --model=models/pdbbind.model

- training on davis

        python yuel.py train --data=data/davis-train.csv \
                             --compound_column=SMILES \
                             --compound_type_column=CompType \
                             --protein_column=Sequence \
                             --affinity_column=Affinity \
                             --cpt_path=checkpoints/davis \
                             --comp_size=64 \
                             --nfeat=32 \
                             --learning_rate=0.00005 \
                             --epochs=200 \
                             --prot_size=2048
                             --model=models/davis.model

# Prediction

- predict pdbbind by pdbbind 

        python yuel.py predict --data=data/pdbbind-test.csv \
                               --compound_column=Ligand \
                               --compound_type_column=CompType \
                               --protein_column=Sequence \
                               --affinity_column=Affinity \
                               --comp_size=64 \
                               --nfeat=32 \
                               --prot_size=2048 \
                               --model=models/pdbbind.model

- predict davis by davis 

        python yuel.py predict --data=data/davis-test.csv \
                               --compound_column=SMILES \
                               --compound_type_column=CompType \
                               --protein_column=Sequence \
                               --affinity_column=Affinity \
                               --comp_size=64 \
                               --nfeat=32 \
                               --prot_size=2048
                               --model=models/davis.model

- predict pdbbind by pdbbind 

        python yuel.py predict --data=data/pdbbind-test.csv \
                               --compound_column=Ligand \
                               --compound_type_column=CompType \
                               --protein_column=Sequence \
                               --affinity_column=Affinity \
                               --comp_size=64 \
                               --nfeat=32 \
                               --prot_size=2048 \
                               --model=models/davis.model

To predict a custom dataset, make sure to designate the compound column, compound type column, protein column, and the affinity column. If the compound column is the SMILES of the compound, then the compound type column is always 'smi'; if the compound column is the Inchi of the compound, then the compound type column is 'inchi'. The protein column is always the sequence of the protein. The 'affinity column' is also needed for the prediction; just assign 0 or any value. Yuel will predict the affinity.

# License

Yuel follow GPL 3.0v license. Yuel are freely available for academic purpose or individual research, but restricted for commecial use.

# Contact

- <jianopt@gmail.com>
- <dokh@psu.edu>

