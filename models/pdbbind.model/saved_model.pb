Ъ╥
╤г
8
Const
output"dtype"
valuetensor"
dtypetype

NoOp
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetypeИ
╛
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring И
Ц
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 И"serve*2.3.02v2.3.0-rc2-23-gb36436b0878св
О
drift/attention/linear/wVarHandleOp*
_output_shapes
: *
dtype0*
shape:
А└*)
shared_namedrift/attention/linear/w
З
,drift/attention/linear/w/Read/ReadVariableOpReadVariableOpdrift/attention/linear/w* 
_output_shapes
:
А└*
dtype0
Й
drift/attention/linear/bVarHandleOp*
_output_shapes
: *
dtype0*
shape:А*)
shared_namedrift/attention/linear/b
В
,drift/attention/linear/b/Read/ReadVariableOpReadVariableOpdrift/attention/linear/b*
_output_shapes	
:А*
dtype0
С
drift/attention/linear/w_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:	 А*+
shared_namedrift/attention/linear/w_1
К
.drift/attention/linear/w_1/Read/ReadVariableOpReadVariableOpdrift/attention/linear/w_1*
_output_shapes
:	 А*
dtype0
Н
drift/attention/linear/b_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:А*+
shared_namedrift/attention/linear/b_1
Ж
.drift/attention/linear/b_1/Read/ReadVariableOpReadVariableOpdrift/attention/linear/b_1*
_output_shapes	
:А*
dtype0
Т
drift/attention/linear/w_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:
АА*+
shared_namedrift/attention/linear/w_2
Л
.drift/attention/linear/w_2/Read/ReadVariableOpReadVariableOpdrift/attention/linear/w_2* 
_output_shapes
:
АА*
dtype0
Н
drift/attention/linear/b_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:А*+
shared_namedrift/attention/linear/b_2
Ж
.drift/attention/linear/b_2/Read/ReadVariableOpReadVariableOpdrift/attention/linear/b_2*
_output_shapes	
:А*
dtype0
Т
drift/attention/linear/w_3VarHandleOp*
_output_shapes
: *
dtype0*
shape:
АА*+
shared_namedrift/attention/linear/w_3
Л
.drift/attention/linear/w_3/Read/ReadVariableOpReadVariableOpdrift/attention/linear/w_3* 
_output_shapes
:
АА*
dtype0
Н
drift/attention/linear/b_3VarHandleOp*
_output_shapes
: *
dtype0*
shape:А*+
shared_namedrift/attention/linear/b_3
Ж
.drift/attention/linear/b_3/Read/ReadVariableOpReadVariableOpdrift/attention/linear/b_3*
_output_shapes	
:А*
dtype0
Т
drift/attention/linear/w_4VarHandleOp*
_output_shapes
: *
dtype0*
shape:
АА*+
shared_namedrift/attention/linear/w_4
Л
.drift/attention/linear/w_4/Read/ReadVariableOpReadVariableOpdrift/attention/linear/w_4* 
_output_shapes
:
АА*
dtype0
Н
drift/attention/linear/b_4VarHandleOp*
_output_shapes
: *
dtype0*
shape:А*+
shared_namedrift/attention/linear/b_4
Ж
.drift/attention/linear/b_4/Read/ReadVariableOpReadVariableOpdrift/attention/linear/b_4*
_output_shapes	
:А*
dtype0
Т
drift/attention/linear/w_5VarHandleOp*
_output_shapes
: *
dtype0*
shape:
АА*+
shared_namedrift/attention/linear/w_5
Л
.drift/attention/linear/w_5/Read/ReadVariableOpReadVariableOpdrift/attention/linear/w_5* 
_output_shapes
:
АА*
dtype0
Т
drift/attention/linear/w_6VarHandleOp*
_output_shapes
: *
dtype0*
shape:
А└*+
shared_namedrift/attention/linear/w_6
Л
.drift/attention/linear/w_6/Read/ReadVariableOpReadVariableOpdrift/attention/linear/w_6* 
_output_shapes
:
А└*
dtype0
Н
drift/attention/linear/b_5VarHandleOp*
_output_shapes
: *
dtype0*
shape:А*+
shared_namedrift/attention/linear/b_5
Ж
.drift/attention/linear/b_5/Read/ReadVariableOpReadVariableOpdrift/attention/linear/b_5*
_output_shapes	
:А*
dtype0
Т
drift/attention/linear/w_7VarHandleOp*
_output_shapes
: *
dtype0*
shape:
АА*+
shared_namedrift/attention/linear/w_7
Л
.drift/attention/linear/w_7/Read/ReadVariableOpReadVariableOpdrift/attention/linear/w_7* 
_output_shapes
:
АА*
dtype0
Н
drift/attention/linear/b_6VarHandleOp*
_output_shapes
: *
dtype0*
shape:А*+
shared_namedrift/attention/linear/b_6
Ж
.drift/attention/linear/b_6/Read/ReadVariableOpReadVariableOpdrift/attention/linear/b_6*
_output_shapes	
:А*
dtype0
Т
drift/attention/linear/w_8VarHandleOp*
_output_shapes
: *
dtype0*
shape:
АА*+
shared_namedrift/attention/linear/w_8
Л
.drift/attention/linear/w_8/Read/ReadVariableOpReadVariableOpdrift/attention/linear/w_8* 
_output_shapes
:
АА*
dtype0
Н
drift/attention/linear/b_7VarHandleOp*
_output_shapes
: *
dtype0*
shape:А*+
shared_namedrift/attention/linear/b_7
Ж
.drift/attention/linear/b_7/Read/ReadVariableOpReadVariableOpdrift/attention/linear/b_7*
_output_shapes	
:А*
dtype0
Т
drift/attention/linear/w_9VarHandleOp*
_output_shapes
: *
dtype0*
shape:
АА*+
shared_namedrift/attention/linear/w_9
Л
.drift/attention/linear/w_9/Read/ReadVariableOpReadVariableOpdrift/attention/linear/w_9* 
_output_shapes
:
АА*
dtype0
Н
drift/attention/linear/b_8VarHandleOp*
_output_shapes
: *
dtype0*
shape:А*+
shared_namedrift/attention/linear/b_8
Ж
.drift/attention/linear/b_8/Read/ReadVariableOpReadVariableOpdrift/attention/linear/b_8*
_output_shapes	
:А*
dtype0
Ф
drift/attention/linear/w_10VarHandleOp*
_output_shapes
: *
dtype0*
shape:
АА*,
shared_namedrift/attention/linear/w_10
Н
/drift/attention/linear/w_10/Read/ReadVariableOpReadVariableOpdrift/attention/linear/w_10* 
_output_shapes
:
АА*
dtype0
Н
drift/attention/linear/b_9VarHandleOp*
_output_shapes
: *
dtype0*
shape:А*+
shared_namedrift/attention/linear/b_9
Ж
.drift/attention/linear/b_9/Read/ReadVariableOpReadVariableOpdrift/attention/linear/b_9*
_output_shapes	
:А*
dtype0
Ф
drift/attention/linear/w_11VarHandleOp*
_output_shapes
: *
dtype0*
shape:
АА*,
shared_namedrift/attention/linear/w_11
Н
/drift/attention/linear/w_11/Read/ReadVariableOpReadVariableOpdrift/attention/linear/w_11* 
_output_shapes
:
АА*
dtype0
ж
'drift/graph_network/edge_block/linear/bVarHandleOp*
_output_shapes
: *
dtype0*
shape: *8
shared_name)'drift/graph_network/edge_block/linear/b
Я
;drift/graph_network/edge_block/linear/b/Read/ReadVariableOpReadVariableOp'drift/graph_network/edge_block/linear/b*
_output_shapes
: *
dtype0
л
'drift/graph_network/edge_block/linear/wVarHandleOp*
_output_shapes
: *
dtype0*
shape:	к *8
shared_name)'drift/graph_network/edge_block/linear/w
д
;drift/graph_network/edge_block/linear/w/Read/ReadVariableOpReadVariableOp'drift/graph_network/edge_block/linear/w*
_output_shapes
:	к *
dtype0
к
)drift/graph_network/global_block/linear/bVarHandleOp*
_output_shapes
: *
dtype0*
shape:*:
shared_name+)drift/graph_network/global_block/linear/b
г
=drift/graph_network/global_block/linear/b/Read/ReadVariableOpReadVariableOp)drift/graph_network/global_block/linear/b*
_output_shapes
:*
dtype0
о
)drift/graph_network/global_block/linear/wVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*:
shared_name+)drift/graph_network/global_block/linear/w
з
=drift/graph_network/global_block/linear/w/Read/ReadVariableOpReadVariableOp)drift/graph_network/global_block/linear/w*
_output_shapes

:@*
dtype0
ж
'drift/graph_network/node_block/linear/bVarHandleOp*
_output_shapes
: *
dtype0*
shape: *8
shared_name)'drift/graph_network/node_block/linear/b
Я
;drift/graph_network/node_block/linear/b/Read/ReadVariableOpReadVariableOp'drift/graph_network/node_block/linear/b*
_output_shapes
: *
dtype0
к
'drift/graph_network/node_block/linear/wVarHandleOp*
_output_shapes
: *
dtype0*
shape
:r *8
shared_name)'drift/graph_network/node_block/linear/w
г
;drift/graph_network/node_block/linear/w/Read/ReadVariableOpReadVariableOp'drift/graph_network/node_block/linear/w*
_output_shapes

:r *
dtype0
к
)drift/graph_network/edge_block/linear/b_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *:
shared_name+)drift/graph_network/edge_block/linear/b_1
г
=drift/graph_network/edge_block/linear/b_1/Read/ReadVariableOpReadVariableOp)drift/graph_network/edge_block/linear/b_1*
_output_shapes
: *
dtype0
о
)drift/graph_network/edge_block/linear/w_1VarHandleOp*
_output_shapes
: *
dtype0*
shape
:a *:
shared_name+)drift/graph_network/edge_block/linear/w_1
з
=drift/graph_network/edge_block/linear/w_1/Read/ReadVariableOpReadVariableOp)drift/graph_network/edge_block/linear/w_1*
_output_shapes

:a *
dtype0
о
+drift/graph_network/global_block/linear/b_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:*<
shared_name-+drift/graph_network/global_block/linear/b_1
з
?drift/graph_network/global_block/linear/b_1/Read/ReadVariableOpReadVariableOp+drift/graph_network/global_block/linear/b_1*
_output_shapes
:*
dtype0
▓
+drift/graph_network/global_block/linear/w_1VarHandleOp*
_output_shapes
: *
dtype0*
shape
:A*<
shared_name-+drift/graph_network/global_block/linear/w_1
л
?drift/graph_network/global_block/linear/w_1/Read/ReadVariableOpReadVariableOp+drift/graph_network/global_block/linear/w_1*
_output_shapes

:A*
dtype0
к
)drift/graph_network/node_block/linear/b_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *:
shared_name+)drift/graph_network/node_block/linear/b_1
г
=drift/graph_network/node_block/linear/b_1/Read/ReadVariableOpReadVariableOp)drift/graph_network/node_block/linear/b_1*
_output_shapes
: *
dtype0
о
)drift/graph_network/node_block/linear/w_1VarHandleOp*
_output_shapes
: *
dtype0*
shape
:A *:
shared_name+)drift/graph_network/node_block/linear/w_1
з
=drift/graph_network/node_block/linear/w_1/Read/ReadVariableOpReadVariableOp)drift/graph_network/node_block/linear/w_1*
_output_shapes

:A *
dtype0
v
drift/conv1_d/bVarHandleOp*
_output_shapes
: *
dtype0*
shape: * 
shared_namedrift/conv1_d/b
o
#drift/conv1_d/b/Read/ReadVariableOpReadVariableOpdrift/conv1_d/b*
_output_shapes
: *
dtype0
~
drift/conv1_d/wVarHandleOp*
_output_shapes
: *
dtype0*
shape:  * 
shared_namedrift/conv1_d/w
w
#drift/conv1_d/w/Read/ReadVariableOpReadVariableOpdrift/conv1_d/w*"
_output_shapes
:  *
dtype0
z
drift/conv1_d/b_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *"
shared_namedrift/conv1_d/b_1
s
%drift/conv1_d/b_1/Read/ReadVariableOpReadVariableOpdrift/conv1_d/b_1*
_output_shapes
: *
dtype0
В
drift/conv1_d/w_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:  *"
shared_namedrift/conv1_d/w_1
{
%drift/conv1_d/w_1/Read/ReadVariableOpReadVariableOpdrift/conv1_d/w_1*"
_output_shapes
:  *
dtype0
z
drift/conv1_d/b_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *"
shared_namedrift/conv1_d/b_2
s
%drift/conv1_d/b_2/Read/ReadVariableOpReadVariableOpdrift/conv1_d/b_2*
_output_shapes
: *
dtype0
В
drift/conv1_d/w_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:  *"
shared_namedrift/conv1_d/w_2
{
%drift/conv1_d/w_2/Read/ReadVariableOpReadVariableOpdrift/conv1_d/w_2*"
_output_shapes
:  *
dtype0
t
drift/linear/bVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namedrift/linear/b
m
"drift/linear/b/Read/ReadVariableOpReadVariableOpdrift/linear/b*
_output_shapes
: *
dtype0
x
drift/linear/wVarHandleOp*
_output_shapes
: *
dtype0*
shape
: *
shared_namedrift/linear/w
q
"drift/linear/w/Read/ReadVariableOpReadVariableOpdrift/linear/w*
_output_shapes

: *
dtype0

NoOpNoOp
▌"
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*Ш"
valueО"BЛ" BД"
#
all_variables

signatures
╞
0
1
2
3
4
5
	6

7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
 29
!30
"31
#32
$33
%34
&35
'36
(37
)38
*39
+40
,41
 
XV
VARIABLE_VALUEdrift/attention/linear/w*all_variables/0/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEdrift/attention/linear/b*all_variables/1/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEdrift/attention/linear/w_1*all_variables/2/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEdrift/attention/linear/b_1*all_variables/3/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEdrift/attention/linear/w_2*all_variables/4/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEdrift/attention/linear/b_2*all_variables/5/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEdrift/attention/linear/w_3*all_variables/6/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEdrift/attention/linear/b_3*all_variables/7/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEdrift/attention/linear/w_4*all_variables/8/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEdrift/attention/linear/b_4*all_variables/9/.ATTRIBUTES/VARIABLE_VALUE
[Y
VARIABLE_VALUEdrift/attention/linear/w_5+all_variables/10/.ATTRIBUTES/VARIABLE_VALUE
[Y
VARIABLE_VALUEdrift/attention/linear/w_6+all_variables/11/.ATTRIBUTES/VARIABLE_VALUE
[Y
VARIABLE_VALUEdrift/attention/linear/b_5+all_variables/12/.ATTRIBUTES/VARIABLE_VALUE
[Y
VARIABLE_VALUEdrift/attention/linear/w_7+all_variables/13/.ATTRIBUTES/VARIABLE_VALUE
[Y
VARIABLE_VALUEdrift/attention/linear/b_6+all_variables/14/.ATTRIBUTES/VARIABLE_VALUE
[Y
VARIABLE_VALUEdrift/attention/linear/w_8+all_variables/15/.ATTRIBUTES/VARIABLE_VALUE
[Y
VARIABLE_VALUEdrift/attention/linear/b_7+all_variables/16/.ATTRIBUTES/VARIABLE_VALUE
[Y
VARIABLE_VALUEdrift/attention/linear/w_9+all_variables/17/.ATTRIBUTES/VARIABLE_VALUE
[Y
VARIABLE_VALUEdrift/attention/linear/b_8+all_variables/18/.ATTRIBUTES/VARIABLE_VALUE
\Z
VARIABLE_VALUEdrift/attention/linear/w_10+all_variables/19/.ATTRIBUTES/VARIABLE_VALUE
[Y
VARIABLE_VALUEdrift/attention/linear/b_9+all_variables/20/.ATTRIBUTES/VARIABLE_VALUE
\Z
VARIABLE_VALUEdrift/attention/linear/w_11+all_variables/21/.ATTRIBUTES/VARIABLE_VALUE
hf
VARIABLE_VALUE'drift/graph_network/edge_block/linear/b+all_variables/22/.ATTRIBUTES/VARIABLE_VALUE
hf
VARIABLE_VALUE'drift/graph_network/edge_block/linear/w+all_variables/23/.ATTRIBUTES/VARIABLE_VALUE
jh
VARIABLE_VALUE)drift/graph_network/global_block/linear/b+all_variables/24/.ATTRIBUTES/VARIABLE_VALUE
jh
VARIABLE_VALUE)drift/graph_network/global_block/linear/w+all_variables/25/.ATTRIBUTES/VARIABLE_VALUE
hf
VARIABLE_VALUE'drift/graph_network/node_block/linear/b+all_variables/26/.ATTRIBUTES/VARIABLE_VALUE
hf
VARIABLE_VALUE'drift/graph_network/node_block/linear/w+all_variables/27/.ATTRIBUTES/VARIABLE_VALUE
jh
VARIABLE_VALUE)drift/graph_network/edge_block/linear/b_1+all_variables/28/.ATTRIBUTES/VARIABLE_VALUE
jh
VARIABLE_VALUE)drift/graph_network/edge_block/linear/w_1+all_variables/29/.ATTRIBUTES/VARIABLE_VALUE
lj
VARIABLE_VALUE+drift/graph_network/global_block/linear/b_1+all_variables/30/.ATTRIBUTES/VARIABLE_VALUE
lj
VARIABLE_VALUE+drift/graph_network/global_block/linear/w_1+all_variables/31/.ATTRIBUTES/VARIABLE_VALUE
jh
VARIABLE_VALUE)drift/graph_network/node_block/linear/b_1+all_variables/32/.ATTRIBUTES/VARIABLE_VALUE
jh
VARIABLE_VALUE)drift/graph_network/node_block/linear/w_1+all_variables/33/.ATTRIBUTES/VARIABLE_VALUE
PN
VARIABLE_VALUEdrift/conv1_d/b+all_variables/34/.ATTRIBUTES/VARIABLE_VALUE
PN
VARIABLE_VALUEdrift/conv1_d/w+all_variables/35/.ATTRIBUTES/VARIABLE_VALUE
RP
VARIABLE_VALUEdrift/conv1_d/b_1+all_variables/36/.ATTRIBUTES/VARIABLE_VALUE
RP
VARIABLE_VALUEdrift/conv1_d/w_1+all_variables/37/.ATTRIBUTES/VARIABLE_VALUE
RP
VARIABLE_VALUEdrift/conv1_d/b_2+all_variables/38/.ATTRIBUTES/VARIABLE_VALUE
RP
VARIABLE_VALUEdrift/conv1_d/w_2+all_variables/39/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEdrift/linear/b+all_variables/40/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEdrift/linear/w+all_variables/41/.ATTRIBUTES/VARIABLE_VALUE
w
serving_default_compPlaceholder*'
_output_shapes
:         R*
dtype0*
shape:         R
y
serving_default_comp_1Placeholder*'
_output_shapes
:         *
dtype0*
shape:         
q
serving_default_comp_2Placeholder*#
_output_shapes
:         *
dtype0*
shape:         
q
serving_default_comp_3Placeholder*#
_output_shapes
:         *
dtype0*
shape:         
u
serving_default_comp_4Placeholder*%
_output_shapes
:          *
dtype0*
shape:          
q
serving_default_comp_5Placeholder*#
_output_shapes
:         *
dtype0*
shape:         
q
serving_default_comp_6Placeholder*#
_output_shapes
:         *
dtype0*
shape:         
Q
serving_default_is_trainingPlaceholder*
_output_shapes
:*
dtype0

|
serving_default_mask_compPlaceholder*'
_output_shapes
:         @*
dtype0*
shape:         @
~
serving_default_mask_protPlaceholder*(
_output_shapes
:         А*
dtype0*
shape:         А
Б
serving_default_protPlaceholder*,
_output_shapes
:         А*
dtype0*!
shape:         А
У
StatefulPartitionedCallStatefulPartitionedCallserving_default_compserving_default_comp_1serving_default_comp_2serving_default_comp_3serving_default_comp_4serving_default_comp_5serving_default_comp_6serving_default_is_trainingserving_default_mask_compserving_default_mask_protserving_default_prot'drift/graph_network/edge_block/linear/w'drift/graph_network/edge_block/linear/b'drift/graph_network/node_block/linear/w'drift/graph_network/node_block/linear/b)drift/graph_network/global_block/linear/w)drift/graph_network/global_block/linear/b)drift/graph_network/edge_block/linear/w_1)drift/graph_network/edge_block/linear/b_1)drift/graph_network/node_block/linear/w_1)drift/graph_network/node_block/linear/b_1+drift/graph_network/global_block/linear/w_1+drift/graph_network/global_block/linear/b_1drift/linear/wdrift/linear/bdrift/conv1_d/wdrift/conv1_d/bdrift/conv1_d/w_1drift/conv1_d/b_1drift/conv1_d/w_2drift/conv1_d/b_2drift/attention/linear/w_1drift/attention/linear/bdrift/attention/linear/w_7drift/attention/linear/b_5drift/attention/linear/w_2drift/attention/linear/b_1drift/attention/linear/w_8drift/attention/linear/b_6drift/attention/linear/w_3drift/attention/linear/b_2drift/attention/linear/w_9drift/attention/linear/b_7drift/attention/linear/w_4drift/attention/linear/b_3drift/attention/linear/w_10drift/attention/linear/b_8drift/attention/linear/w_5drift/attention/linear/b_4drift/attention/linear/w_11drift/attention/linear/b_9drift/attention/linear/wdrift/attention/linear/w_6*@
Tin9
725
*
Tout
2*
_collective_manager_ids
 *;
_output_shapes)
':         :         @А*L
_read_only_resource_inputs.
,* !"#$%&'()*+,-./01234*0
config_proto 

CPU

GPU2*0J 8В *.
f)R'
%__inference_signature_wrapper_1126708
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
Е
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename,drift/attention/linear/w/Read/ReadVariableOp,drift/attention/linear/b/Read/ReadVariableOp.drift/attention/linear/w_1/Read/ReadVariableOp.drift/attention/linear/b_1/Read/ReadVariableOp.drift/attention/linear/w_2/Read/ReadVariableOp.drift/attention/linear/b_2/Read/ReadVariableOp.drift/attention/linear/w_3/Read/ReadVariableOp.drift/attention/linear/b_3/Read/ReadVariableOp.drift/attention/linear/w_4/Read/ReadVariableOp.drift/attention/linear/b_4/Read/ReadVariableOp.drift/attention/linear/w_5/Read/ReadVariableOp.drift/attention/linear/w_6/Read/ReadVariableOp.drift/attention/linear/b_5/Read/ReadVariableOp.drift/attention/linear/w_7/Read/ReadVariableOp.drift/attention/linear/b_6/Read/ReadVariableOp.drift/attention/linear/w_8/Read/ReadVariableOp.drift/attention/linear/b_7/Read/ReadVariableOp.drift/attention/linear/w_9/Read/ReadVariableOp.drift/attention/linear/b_8/Read/ReadVariableOp/drift/attention/linear/w_10/Read/ReadVariableOp.drift/attention/linear/b_9/Read/ReadVariableOp/drift/attention/linear/w_11/Read/ReadVariableOp;drift/graph_network/edge_block/linear/b/Read/ReadVariableOp;drift/graph_network/edge_block/linear/w/Read/ReadVariableOp=drift/graph_network/global_block/linear/b/Read/ReadVariableOp=drift/graph_network/global_block/linear/w/Read/ReadVariableOp;drift/graph_network/node_block/linear/b/Read/ReadVariableOp;drift/graph_network/node_block/linear/w/Read/ReadVariableOp=drift/graph_network/edge_block/linear/b_1/Read/ReadVariableOp=drift/graph_network/edge_block/linear/w_1/Read/ReadVariableOp?drift/graph_network/global_block/linear/b_1/Read/ReadVariableOp?drift/graph_network/global_block/linear/w_1/Read/ReadVariableOp=drift/graph_network/node_block/linear/b_1/Read/ReadVariableOp=drift/graph_network/node_block/linear/w_1/Read/ReadVariableOp#drift/conv1_d/b/Read/ReadVariableOp#drift/conv1_d/w/Read/ReadVariableOp%drift/conv1_d/b_1/Read/ReadVariableOp%drift/conv1_d/w_1/Read/ReadVariableOp%drift/conv1_d/b_2/Read/ReadVariableOp%drift/conv1_d/w_2/Read/ReadVariableOp"drift/linear/b/Read/ReadVariableOp"drift/linear/w/Read/ReadVariableOpConst*7
Tin0
.2,*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *)
f$R"
 __inference__traced_save_1126868
╕
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamedrift/attention/linear/wdrift/attention/linear/bdrift/attention/linear/w_1drift/attention/linear/b_1drift/attention/linear/w_2drift/attention/linear/b_2drift/attention/linear/w_3drift/attention/linear/b_3drift/attention/linear/w_4drift/attention/linear/b_4drift/attention/linear/w_5drift/attention/linear/w_6drift/attention/linear/b_5drift/attention/linear/w_7drift/attention/linear/b_6drift/attention/linear/w_8drift/attention/linear/b_7drift/attention/linear/w_9drift/attention/linear/b_8drift/attention/linear/w_10drift/attention/linear/b_9drift/attention/linear/w_11'drift/graph_network/edge_block/linear/b'drift/graph_network/edge_block/linear/w)drift/graph_network/global_block/linear/b)drift/graph_network/global_block/linear/w'drift/graph_network/node_block/linear/b'drift/graph_network/node_block/linear/w)drift/graph_network/edge_block/linear/b_1)drift/graph_network/edge_block/linear/w_1+drift/graph_network/global_block/linear/b_1+drift/graph_network/global_block/linear/w_1)drift/graph_network/node_block/linear/b_1)drift/graph_network/node_block/linear/w_1drift/conv1_d/bdrift/conv1_d/wdrift/conv1_d/b_1drift/conv1_d/w_1drift/conv1_d/b_2drift/conv1_d/w_2drift/linear/bdrift/linear/w*6
Tin/
-2+*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *,
f'R%
#__inference__traced_restore_1127004■х
щд
─
__inference_inference_1126605
comp

comp_1

comp_2

comp_3

comp_4

comp_5

comp_6
prot
	mask_comp
	mask_prot
is_training
H
Ddrift_graph_network_edge_block_linear_matmul_readvariableop_resourceE
Adrift_graph_network_edge_block_linear_add_readvariableop_resourceH
Ddrift_graph_network_node_block_linear_matmul_readvariableop_resourceE
Adrift_graph_network_node_block_linear_add_readvariableop_resourceJ
Fdrift_graph_network_global_block_linear_matmul_readvariableop_resourceG
Cdrift_graph_network_global_block_linear_add_readvariableop_resourceJ
Fdrift_graph_network_edge_block_linear_matmul_1_readvariableop_resourceG
Cdrift_graph_network_edge_block_linear_add_1_readvariableop_resourceJ
Fdrift_graph_network_node_block_linear_matmul_1_readvariableop_resourceG
Cdrift_graph_network_node_block_linear_add_1_readvariableop_resourceL
Hdrift_graph_network_global_block_linear_matmul_1_readvariableop_resourceI
Edrift_graph_network_global_block_linear_add_1_readvariableop_resource/
+drift_linear_matmul_readvariableop_resource,
(drift_linear_add_readvariableop_resourceB
>drift_conv1_d_convolution_expanddims_1_readvariableop_resource1
-drift_conv1_d_biasadd_readvariableop_resourceD
@drift_conv1_d_convolution_1_expanddims_1_readvariableop_resource3
/drift_conv1_d_biasadd_1_readvariableop_resourceD
@drift_conv1_d_convolution_2_expanddims_1_readvariableop_resource3
/drift_conv1_d_biasadd_2_readvariableop_resource9
5drift_attention_linear_matmul_readvariableop_resource6
2drift_attention_linear_add_readvariableop_resource;
7drift_attention_linear_matmul_1_readvariableop_resource8
4drift_attention_linear_add_1_readvariableop_resource;
7drift_attention_linear_matmul_2_readvariableop_resource8
4drift_attention_linear_add_2_readvariableop_resource;
7drift_attention_linear_matmul_3_readvariableop_resource8
4drift_attention_linear_add_3_readvariableop_resource;
7drift_attention_linear_matmul_4_readvariableop_resource8
4drift_attention_linear_add_4_readvariableop_resource;
7drift_attention_linear_matmul_5_readvariableop_resource8
4drift_attention_linear_add_5_readvariableop_resource;
7drift_attention_linear_matmul_6_readvariableop_resource8
4drift_attention_linear_add_6_readvariableop_resource;
7drift_attention_linear_matmul_7_readvariableop_resource8
4drift_attention_linear_add_7_readvariableop_resource;
7drift_attention_linear_matmul_8_readvariableop_resource8
4drift_attention_linear_add_8_readvariableop_resource;
7drift_attention_linear_matmul_9_readvariableop_resource8
4drift_attention_linear_add_9_readvariableop_resource<
8drift_attention_linear_matmul_10_readvariableop_resource<
8drift_attention_linear_matmul_11_readvariableop_resource
identity

identity_1Ивdrift/dropout/condт
Ndrift/graph_network/edge_block/broadcast_receiver_nodes_to_edges/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2P
Ndrift/graph_network/edge_block/broadcast_receiver_nodes_to_edges/GatherV2/axis▐
Idrift/graph_network/edge_block/broadcast_receiver_nodes_to_edges/GatherV2GatherV2compcomp_2Wdrift/graph_network/edge_block/broadcast_receiver_nodes_to_edges/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*'
_output_shapes
:         R2K
Idrift/graph_network/edge_block/broadcast_receiver_nodes_to_edges/GatherV2▐
Ldrift/graph_network/edge_block/broadcast_sender_nodes_to_edges/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2N
Ldrift/graph_network/edge_block/broadcast_sender_nodes_to_edges/GatherV2/axis╪
Gdrift/graph_network/edge_block/broadcast_sender_nodes_to_edges/GatherV2GatherV2compcomp_3Udrift/graph_network/edge_block/broadcast_sender_nodes_to_edges/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*'
_output_shapes
:         R2I
Gdrift/graph_network/edge_block/broadcast_sender_nodes_to_edges/GatherV2█
Edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/CastCastcomp_6*

DstT0*

SrcT0*#
_output_shapes
:         2G
Edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Cast╞
Fdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/ShapeShapecomp_4*
T0*
_output_shapes
:2H
Fdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/ShapeЎ
Tdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2V
Tdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/strided_slice/stack·
Vdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2X
Vdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/strided_slice/stack_1·
Vdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2X
Vdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/strided_slice/stack_2ш
Ndrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/strided_sliceStridedSliceOdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Shape:output:0]drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/strided_slice/stack:output:0_drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/strided_slice/stack_1:output:0_drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2P
Ndrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/strided_slice╖
Rdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/BroadcastTo/shapePackWdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/strided_slice:output:0*
N*
T0*
_output_shapes
:2T
Rdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/BroadcastTo/shapeБ
Ldrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/BroadcastToBroadcastToIdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Cast:y:0[drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/BroadcastTo/shape:output:0*
T0*#
_output_shapes
:         2N
Ldrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/BroadcastTo┌
Fdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2H
Fdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Const▄
Ddrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/MaxMaxUdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/BroadcastTo:output:0Odrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Const:output:0*
T0*
_output_shapes
: 2F
Ddrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Max┌
Jdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Maximum/xConst*
_output_shapes
: *
dtype0*
value	B : 2L
Jdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Maximum/xф
Hdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/MaximumMaximumSdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Maximum/x:output:0Mdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Max:output:0*
T0*
_output_shapes
: 2J
Hdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Maximumь
Sdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/ConstConst*
_output_shapes
: *
dtype0*
value	B : 2U
Sdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/ConstЁ
Udrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/Const_1Const*
_output_shapes
: *
dtype0*
value	B :2W
Udrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/Const_1ф
Sdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/RangeRange\drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/Const:output:0Ldrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Maximum:z:0^drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/Const_1:output:0*#
_output_shapes
:         2U
Sdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/RangeЗ
\drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2^
\drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/ExpandDims/dim▓
Xdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/ExpandDims
ExpandDimsUdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/BroadcastTo:output:0edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2Z
Xdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/ExpandDims╘
Rdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/CastCastadrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/ExpandDims:output:0*

DstT0*

SrcT0*'
_output_shapes
:         2T
Rdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/Castб
Rdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/LessLess\drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/Range:output:0Vdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/Cast:y:0*
T0*0
_output_shapes
:                  2T
Rdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/Lessф
Odrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :2Q
Odrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/ExpandDims/dim╛
Kdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/ExpandDims
ExpandDimscomp_4Xdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/ExpandDims/dim:output:0*
T0*)
_output_shapes
:          2M
Kdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/ExpandDimsш
Qdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile/multiples/0Const*
_output_shapes
: *
dtype0*
value	B :2S
Qdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile/multiples/0ш
Qdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile/multiples/2Const*
_output_shapes
: *
dtype0*
value	B :2S
Qdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile/multiples/2▐
Odrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile/multiplesPackZdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile/multiples/0:output:0Ldrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Maximum:z:0Zdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile/multiples/2:output:0*
N*
T0*
_output_shapes
:2Q
Odrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile/multiplesГ
Edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/TileTileTdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/ExpandDims:output:0Xdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile/multiples:output:0*
T0*2
_output_shapes 
:                   2G
Edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tileи
Sdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/ShapeShapeNdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile:output:0*
T0*
_output_shapes
:2U
Sdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/ShapeР
adrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2c
adrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice/stackФ
cdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2e
cdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice/stack_1Ф
cdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2e
cdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice/stack_2в
[drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_sliceStridedSlice\drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Shape:output:0jdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice/stack:output:0ldrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice/stack_1:output:0ldrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:2]
[drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_sliceЦ
ddrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Prod/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2f
ddrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Prod/reduction_indicesж
Rdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/ProdProdddrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice:output:0mdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Prod/reduction_indices:output:0*
T0*
_output_shapes
: 2T
Rdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Prodм
Udrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Shape_1ShapeNdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile:output:0*
T0*
_output_shapes
:2W
Udrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Shape_1Ф
cdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2e
cdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_1/stackШ
edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2g
edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_1/stack_1Ш
edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2g
edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_1/stack_2╛
]drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_1StridedSlice^drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Shape_1:output:0ldrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_1/stack:output:0ndrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_1/stack_1:output:0ndrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *

begin_mask2_
]drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_1м
Udrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Shape_2ShapeNdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile:output:0*
T0*
_output_shapes
:2W
Udrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Shape_2Ф
cdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2e
cdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_2/stackШ
edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2g
edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_2/stack_1Ш
edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2g
edrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_2/stack_2╛
]drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_2StridedSlice^drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Shape_2:output:0ldrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_2/stack:output:0ndrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_2/stack_1:output:0ndrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
:*
end_mask2_
]drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_2╤
]drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/concat/values_1Pack[drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Prod:output:0*
N*
T0*
_output_shapes
:2_
]drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/concat/values_1°
Ydrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2[
Ydrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/concat/axisВ
Tdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/concatConcatV2fdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_1:output:0fdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/concat/values_1:output:0fdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/strided_slice_2:output:0bdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/concat/axis:output:0*
N*
T0*
_output_shapes
:2V
Tdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/concatШ
Udrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/ReshapeReshapeNdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/Tile:output:0]drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/concat:output:0*
T0*%
_output_shapes
:          2W
Udrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/ReshapeС
]drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:
         2_
]drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Reshape_1/shapeл
Wdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Reshape_1ReshapeVdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/SequenceMask/Less:z:0fdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Reshape_1/shape:output:0*
T0
*#
_output_shapes
:         2Y
Wdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Reshape_1╛
Sdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/WhereWhere`drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Reshape_1:output:0*'
_output_shapes
:         2U
Sdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Where█
Udrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/SqueezeSqueeze[drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Where:index:0*
T0	*#
_output_shapes
:         *
squeeze_dims
2W
Udrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Squeeze№
[drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2]
[drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/GatherV2/axis╡
Vdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/GatherV2GatherV2^drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Reshape:output:0^drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/Squeeze:output:0ddrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/GatherV2/axis:output:0*
Taxis0*
Tindices0	*
Tparams0*%
_output_shapes
:          2X
Vdrift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/GatherV2г
*drift/graph_network/edge_block/concat/axisConst*
_output_shapes
: *
dtype0*
valueB :
         2,
*drift/graph_network/edge_block/concat/axis┌
%drift/graph_network/edge_block/concatConcatV2comp_1Rdrift/graph_network/edge_block/broadcast_receiver_nodes_to_edges/GatherV2:output:0Pdrift/graph_network/edge_block/broadcast_sender_nodes_to_edges/GatherV2:output:0_drift/graph_network/edge_block/broadcast_globals_to_edges/repeat/boolean_mask/GatherV2:output:03drift/graph_network/edge_block/concat/axis:output:0*
N*
T0*(
_output_shapes
:         к2'
%drift/graph_network/edge_block/concatА
;drift/graph_network/edge_block/linear/MatMul/ReadVariableOpReadVariableOpDdrift_graph_network_edge_block_linear_matmul_readvariableop_resource*
_output_shapes
:	к *
dtype02=
;drift/graph_network/edge_block/linear/MatMul/ReadVariableOpН
,drift/graph_network/edge_block/linear/MatMulMatMul.drift/graph_network/edge_block/concat:output:0Cdrift/graph_network/edge_block/linear/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:          2.
,drift/graph_network/edge_block/linear/MatMulЄ
8drift/graph_network/edge_block/linear/Add/ReadVariableOpReadVariableOpAdrift_graph_network_edge_block_linear_add_readvariableop_resource*
_output_shapes
: *
dtype02:
8drift/graph_network/edge_block/linear/Add/ReadVariableOpЙ
)drift/graph_network/edge_block/linear/AddAdd6drift/graph_network/edge_block/linear/MatMul:product:0@drift/graph_network/edge_block/linear/Add/ReadVariableOp:value:0*
T0*'
_output_shapes
:          2+
)drift/graph_network/edge_block/linear/Add▄
Gdrift/graph_network/node_block/received_edges_to_nodes_aggregator/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2I
Gdrift/graph_network/node_block/received_edges_to_nodes_aggregator/ConstР
Edrift/graph_network/node_block/received_edges_to_nodes_aggregator/SumSumcomp_5Pdrift/graph_network/node_block/received_edges_to_nodes_aggregator/Const:output:0*
T0*
_output_shapes
: 2G
Edrift/graph_network/node_block/received_edges_to_nodes_aggregator/SumЛ
Tdrift/graph_network/node_block/received_edges_to_nodes_aggregator/UnsortedSegmentSumUnsortedSegmentSum-drift/graph_network/edge_block/linear/Add:z:0comp_2Ndrift/graph_network/node_block/received_edges_to_nodes_aggregator/Sum:output:0*
T0*
Tindices0*'
_output_shapes
:          2V
Tdrift/graph_network/node_block/received_edges_to_nodes_aggregator/UnsortedSegmentSum█
Edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/CastCastcomp_5*

DstT0*

SrcT0*#
_output_shapes
:         2G
Edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Cast╞
Fdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/ShapeShapecomp_4*
T0*
_output_shapes
:2H
Fdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/ShapeЎ
Tdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2V
Tdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/strided_slice/stack·
Vdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2X
Vdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/strided_slice/stack_1·
Vdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2X
Vdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/strided_slice/stack_2ш
Ndrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/strided_sliceStridedSliceOdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Shape:output:0]drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/strided_slice/stack:output:0_drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/strided_slice/stack_1:output:0_drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2P
Ndrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/strided_slice╖
Rdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/BroadcastTo/shapePackWdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/strided_slice:output:0*
N*
T0*
_output_shapes
:2T
Rdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/BroadcastTo/shapeБ
Ldrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/BroadcastToBroadcastToIdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Cast:y:0[drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/BroadcastTo/shape:output:0*
T0*#
_output_shapes
:         2N
Ldrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/BroadcastTo┌
Fdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2H
Fdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Const▄
Ddrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/MaxMaxUdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/BroadcastTo:output:0Odrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Const:output:0*
T0*
_output_shapes
: 2F
Ddrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Max┌
Jdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Maximum/xConst*
_output_shapes
: *
dtype0*
value	B : 2L
Jdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Maximum/xф
Hdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/MaximumMaximumSdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Maximum/x:output:0Mdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Max:output:0*
T0*
_output_shapes
: 2J
Hdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Maximumь
Sdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/ConstConst*
_output_shapes
: *
dtype0*
value	B : 2U
Sdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/ConstЁ
Udrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/Const_1Const*
_output_shapes
: *
dtype0*
value	B :2W
Udrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/Const_1ф
Sdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/RangeRange\drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/Const:output:0Ldrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Maximum:z:0^drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/Const_1:output:0*#
_output_shapes
:         2U
Sdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/RangeЗ
\drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2^
\drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/ExpandDims/dim▓
Xdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/ExpandDims
ExpandDimsUdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/BroadcastTo:output:0edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2Z
Xdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/ExpandDims╘
Rdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/CastCastadrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/ExpandDims:output:0*

DstT0*

SrcT0*'
_output_shapes
:         2T
Rdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/Castб
Rdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/LessLess\drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/Range:output:0Vdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/Cast:y:0*
T0*0
_output_shapes
:                  2T
Rdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/Lessф
Odrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :2Q
Odrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/ExpandDims/dim╛
Kdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/ExpandDims
ExpandDimscomp_4Xdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/ExpandDims/dim:output:0*
T0*)
_output_shapes
:          2M
Kdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/ExpandDimsш
Qdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile/multiples/0Const*
_output_shapes
: *
dtype0*
value	B :2S
Qdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile/multiples/0ш
Qdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile/multiples/2Const*
_output_shapes
: *
dtype0*
value	B :2S
Qdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile/multiples/2▐
Odrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile/multiplesPackZdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile/multiples/0:output:0Ldrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Maximum:z:0Zdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile/multiples/2:output:0*
N*
T0*
_output_shapes
:2Q
Odrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile/multiplesГ
Edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/TileTileTdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/ExpandDims:output:0Xdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile/multiples:output:0*
T0*2
_output_shapes 
:                   2G
Edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tileи
Sdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/ShapeShapeNdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile:output:0*
T0*
_output_shapes
:2U
Sdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/ShapeР
adrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2c
adrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice/stackФ
cdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2e
cdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice/stack_1Ф
cdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2e
cdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice/stack_2в
[drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_sliceStridedSlice\drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Shape:output:0jdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice/stack:output:0ldrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice/stack_1:output:0ldrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:2]
[drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_sliceЦ
ddrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Prod/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2f
ddrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Prod/reduction_indicesж
Rdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/ProdProdddrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice:output:0mdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Prod/reduction_indices:output:0*
T0*
_output_shapes
: 2T
Rdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Prodм
Udrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Shape_1ShapeNdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile:output:0*
T0*
_output_shapes
:2W
Udrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Shape_1Ф
cdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2e
cdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_1/stackШ
edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2g
edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_1/stack_1Ш
edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2g
edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_1/stack_2╛
]drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_1StridedSlice^drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Shape_1:output:0ldrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_1/stack:output:0ndrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_1/stack_1:output:0ndrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *

begin_mask2_
]drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_1м
Udrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Shape_2ShapeNdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile:output:0*
T0*
_output_shapes
:2W
Udrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Shape_2Ф
cdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2e
cdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_2/stackШ
edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2g
edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_2/stack_1Ш
edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2g
edrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_2/stack_2╛
]drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_2StridedSlice^drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Shape_2:output:0ldrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_2/stack:output:0ndrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_2/stack_1:output:0ndrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
:*
end_mask2_
]drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_2╤
]drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/concat/values_1Pack[drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Prod:output:0*
N*
T0*
_output_shapes
:2_
]drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/concat/values_1°
Ydrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2[
Ydrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/concat/axisВ
Tdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/concatConcatV2fdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_1:output:0fdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/concat/values_1:output:0fdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/strided_slice_2:output:0bdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/concat/axis:output:0*
N*
T0*
_output_shapes
:2V
Tdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/concatШ
Udrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/ReshapeReshapeNdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/Tile:output:0]drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/concat:output:0*
T0*%
_output_shapes
:          2W
Udrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/ReshapeС
]drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:
         2_
]drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Reshape_1/shapeл
Wdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Reshape_1ReshapeVdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/SequenceMask/Less:z:0fdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Reshape_1/shape:output:0*
T0
*#
_output_shapes
:         2Y
Wdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Reshape_1╛
Sdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/WhereWhere`drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Reshape_1:output:0*'
_output_shapes
:         2U
Sdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Where█
Udrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/SqueezeSqueeze[drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Where:index:0*
T0	*#
_output_shapes
:         *
squeeze_dims
2W
Udrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Squeeze№
[drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2]
[drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/GatherV2/axis╡
Vdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/GatherV2GatherV2^drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Reshape:output:0^drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/Squeeze:output:0ddrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/GatherV2/axis:output:0*
Taxis0*
Tindices0	*
Tparams0*%
_output_shapes
:          2X
Vdrift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/GatherV2г
*drift/graph_network/node_block/concat/axisConst*
_output_shapes
: *
dtype0*
valueB :
         2,
*drift/graph_network/node_block/concat/axisР
%drift/graph_network/node_block/concatConcatV2]drift/graph_network/node_block/received_edges_to_nodes_aggregator/UnsortedSegmentSum:output:0comp_drift/graph_network/node_block/broadcast_globals_to_nodes/repeat/boolean_mask/GatherV2:output:03drift/graph_network/node_block/concat/axis:output:0*
N*
T0*'
_output_shapes
:         r2'
%drift/graph_network/node_block/concat 
;drift/graph_network/node_block/linear/MatMul/ReadVariableOpReadVariableOpDdrift_graph_network_node_block_linear_matmul_readvariableop_resource*
_output_shapes

:r *
dtype02=
;drift/graph_network/node_block/linear/MatMul/ReadVariableOpН
,drift/graph_network/node_block/linear/MatMulMatMul.drift/graph_network/node_block/concat:output:0Cdrift/graph_network/node_block/linear/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:          2.
,drift/graph_network/node_block/linear/MatMulЄ
8drift/graph_network/node_block/linear/Add/ReadVariableOpReadVariableOpAdrift_graph_network_node_block_linear_add_readvariableop_resource*
_output_shapes
: *
dtype02:
8drift/graph_network/node_block/linear/Add/ReadVariableOpЙ
)drift/graph_network/node_block/linear/AddAdd6drift/graph_network/node_block/linear/MatMul:product:0@drift/graph_network/node_block/linear/Add/ReadVariableOp:value:0*
T0*'
_output_shapes
:          2+
)drift/graph_network/node_block/linear/Add▄
Qdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/ShapeShapecomp_5*
T0*
_output_shapes
:2S
Qdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/ShapeМ
_drift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2a
_drift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_slice/stackР
adrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2c
adrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_slice/stack_1Р
adrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2c
adrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_slice/stack_2к
Ydrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_sliceStridedSliceZdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/Shape:output:0hdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_slice/stack:output:0jdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_slice/stack_1:output:0jdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2[
Ydrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_slice╓
Hdrift/graph_network/global_block/edges_to_globals_aggregator/range/startConst*
_output_shapes
: *
dtype0*
value	B : 2J
Hdrift/graph_network/global_block/edges_to_globals_aggregator/range/start╓
Hdrift/graph_network/global_block/edges_to_globals_aggregator/range/deltaConst*
_output_shapes
: *
dtype0*
value	B :2J
Hdrift/graph_network/global_block/edges_to_globals_aggregator/range/delta└
Bdrift/graph_network/global_block/edges_to_globals_aggregator/rangeRangeQdrift/graph_network/global_block/edges_to_globals_aggregator/range/start:output:0bdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_slice:output:0Qdrift/graph_network/global_block/edges_to_globals_aggregator/range/delta:output:0*#
_output_shapes
:         2D
Bdrift/graph_network/global_block/edges_to_globals_aggregator/rangeс
Hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/CastCastcomp_6*

DstT0*

SrcT0*#
_output_shapes
:         2J
Hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/CastС
Idrift/graph_network/global_block/edges_to_globals_aggregator/repeat/ShapeShapeKdrift/graph_network/global_block/edges_to_globals_aggregator/range:output:0*
T0*
_output_shapes
:2K
Idrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Shape№
Wdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2Y
Wdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/strided_slice/stackА
Ydrift/graph_network/global_block/edges_to_globals_aggregator/repeat/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2[
Ydrift/graph_network/global_block/edges_to_globals_aggregator/repeat/strided_slice/stack_1А
Ydrift/graph_network/global_block/edges_to_globals_aggregator/repeat/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2[
Ydrift/graph_network/global_block/edges_to_globals_aggregator/repeat/strided_slice/stack_2·
Qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/strided_sliceStridedSliceRdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Shape:output:0`drift/graph_network/global_block/edges_to_globals_aggregator/repeat/strided_slice/stack:output:0bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/strided_slice/stack_1:output:0bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2S
Qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/strided_slice└
Udrift/graph_network/global_block/edges_to_globals_aggregator/repeat/BroadcastTo/shapePackZdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/strided_slice:output:0*
N*
T0*
_output_shapes
:2W
Udrift/graph_network/global_block/edges_to_globals_aggregator/repeat/BroadcastTo/shapeН
Odrift/graph_network/global_block/edges_to_globals_aggregator/repeat/BroadcastToBroadcastToLdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Cast:y:0^drift/graph_network/global_block/edges_to_globals_aggregator/repeat/BroadcastTo/shape:output:0*
T0*#
_output_shapes
:         2Q
Odrift/graph_network/global_block/edges_to_globals_aggregator/repeat/BroadcastToр
Idrift/graph_network/global_block/edges_to_globals_aggregator/repeat/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2K
Idrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Constш
Gdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/MaxMaxXdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/BroadcastTo:output:0Rdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Const:output:0*
T0*
_output_shapes
: 2I
Gdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Maxр
Mdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Maximum/xConst*
_output_shapes
: *
dtype0*
value	B : 2O
Mdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Maximum/xЁ
Kdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/MaximumMaximumVdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Maximum/x:output:0Pdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Max:output:0*
T0*
_output_shapes
: 2M
Kdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/MaximumЄ
Vdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/ConstConst*
_output_shapes
: *
dtype0*
value	B : 2X
Vdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/ConstЎ
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/Const_1Const*
_output_shapes
: *
dtype0*
value	B :2Z
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/Const_1є
Vdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/RangeRange_drift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/Const:output:0Odrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Maximum:z:0adrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/Const_1:output:0*#
_output_shapes
:         2X
Vdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/RangeН
_drift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2a
_drift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/ExpandDims/dim╛
[drift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/ExpandDims
ExpandDimsXdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/BroadcastTo:output:0hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2]
[drift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/ExpandDims▌
Udrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/CastCastddrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/ExpandDims:output:0*

DstT0*

SrcT0*'
_output_shapes
:         2W
Udrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/Castн
Udrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/LessLess_drift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/Range:output:0Ydrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/Cast:y:0*
T0*0
_output_shapes
:                  2W
Udrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/Lessъ
Rdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :2T
Rdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/ExpandDims/dimК
Ndrift/graph_network/global_block/edges_to_globals_aggregator/repeat/ExpandDims
ExpandDimsKdrift/graph_network/global_block/edges_to_globals_aggregator/range:output:0[drift/graph_network/global_block/edges_to_globals_aggregator/repeat/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2P
Ndrift/graph_network/global_block/edges_to_globals_aggregator/repeat/ExpandDimsю
Tdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Tile/multiples/0Const*
_output_shapes
: *
dtype0*
value	B :2V
Tdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Tile/multiples/0О
Rdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Tile/multiplesPack]drift/graph_network/global_block/edges_to_globals_aggregator/repeat/Tile/multiples/0:output:0Odrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Maximum:z:0*
N*
T0*
_output_shapes
:2T
Rdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Tile/multiplesН
Hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/TileTileWdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/ExpandDims:output:0[drift/graph_network/global_block/edges_to_globals_aggregator/repeat/Tile/multiples:output:0*
T0*0
_output_shapes
:                  2J
Hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Tile▒
Vdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/ShapeShapeQdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Tile:output:0*
T0*
_output_shapes
:2X
Vdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/ShapeЦ
ddrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2f
ddrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice/stackЪ
fdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2h
fdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack_1Ъ
fdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2h
fdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack_2┤
^drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_sliceStridedSlice_drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Shape:output:0mdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack:output:0odrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack_1:output:0odrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:2`
^drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_sliceЬ
gdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Prod/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2i
gdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Prod/reduction_indices▓
Udrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/ProdProdgdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice:output:0pdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Prod/reduction_indices:output:0*
T0*
_output_shapes
: 2W
Udrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Prod╡
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Shape_1ShapeQdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Tile:output:0*
T0*
_output_shapes
:2Z
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Shape_1Ъ
fdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2h
fdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stackЮ
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2j
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack_1Ю
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2j
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack_2╨
`drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_1StridedSliceadrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Shape_1:output:0odrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack:output:0qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack_1:output:0qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *

begin_mask2b
`drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_1╡
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Shape_2ShapeQdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Tile:output:0*
T0*
_output_shapes
:2Z
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Shape_2Ъ
fdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2h
fdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stackЮ
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2j
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack_1Ю
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2j
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack_2╬
`drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_2StridedSliceadrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Shape_2:output:0odrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack:output:0qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack_1:output:0qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
end_mask2b
`drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_2┌
`drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/concat/values_1Pack^drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Prod:output:0*
N*
T0*
_output_shapes
:2b
`drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/concat/values_1■
\drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2^
\drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/concat/axisФ
Wdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/concatConcatV2idrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_1:output:0idrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/concat/values_1:output:0idrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/strided_slice_2:output:0edrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/concat/axis:output:0*
N*
T0*
_output_shapes
:2Y
Wdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/concatв
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/ReshapeReshapeQdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/Tile:output:0`drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/concat:output:0*
T0*#
_output_shapes
:         2Z
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/ReshapeЧ
`drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:
         2b
`drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Reshape_1/shape╖
Zdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Reshape_1ReshapeYdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/SequenceMask/Less:z:0idrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Reshape_1/shape:output:0*
T0
*#
_output_shapes
:         2\
Zdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Reshape_1╟
Vdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/WhereWherecdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Reshape_1:output:0*'
_output_shapes
:         2X
Vdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Whereф
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/SqueezeSqueeze^drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Where:index:0*
T0	*#
_output_shapes
:         *
squeeze_dims
2Z
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/SqueezeВ
^drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2`
^drift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/GatherV2/axis┬
Ydrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/GatherV2GatherV2adrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Reshape:output:0adrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/Squeeze:output:0gdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/GatherV2/axis:output:0*
Taxis0*
Tindices0	*
Tparams0*#
_output_shapes
:         2[
Ydrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/GatherV2ё
Odrift/graph_network/global_block/edges_to_globals_aggregator/UnsortedSegmentSumUnsortedSegmentSum-drift/graph_network/edge_block/linear/Add:z:0bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat/boolean_mask/GatherV2:output:0bdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs/strided_slice:output:0*
T0*
Tindices0*'
_output_shapes
:          2Q
Odrift/graph_network/global_block/edges_to_globals_aggregator/UnsortedSegmentSum▄
Qdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/ShapeShapecomp_5*
T0*
_output_shapes
:2S
Qdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/ShapeМ
_drift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2a
_drift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_slice/stackР
adrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2c
adrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_slice/stack_1Р
adrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2c
adrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_slice/stack_2к
Ydrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_sliceStridedSliceZdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/Shape:output:0hdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_slice/stack:output:0jdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_slice/stack_1:output:0jdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2[
Ydrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_slice╓
Hdrift/graph_network/global_block/nodes_to_globals_aggregator/range/startConst*
_output_shapes
: *
dtype0*
value	B : 2J
Hdrift/graph_network/global_block/nodes_to_globals_aggregator/range/start╓
Hdrift/graph_network/global_block/nodes_to_globals_aggregator/range/deltaConst*
_output_shapes
: *
dtype0*
value	B :2J
Hdrift/graph_network/global_block/nodes_to_globals_aggregator/range/delta└
Bdrift/graph_network/global_block/nodes_to_globals_aggregator/rangeRangeQdrift/graph_network/global_block/nodes_to_globals_aggregator/range/start:output:0bdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_slice:output:0Qdrift/graph_network/global_block/nodes_to_globals_aggregator/range/delta:output:0*#
_output_shapes
:         2D
Bdrift/graph_network/global_block/nodes_to_globals_aggregator/rangeс
Hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/CastCastcomp_5*

DstT0*

SrcT0*#
_output_shapes
:         2J
Hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/CastС
Idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/ShapeShapeKdrift/graph_network/global_block/nodes_to_globals_aggregator/range:output:0*
T0*
_output_shapes
:2K
Idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Shape№
Wdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2Y
Wdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/strided_slice/stackА
Ydrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2[
Ydrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/strided_slice/stack_1А
Ydrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2[
Ydrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/strided_slice/stack_2·
Qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/strided_sliceStridedSliceRdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Shape:output:0`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/strided_slice/stack:output:0bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/strided_slice/stack_1:output:0bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2S
Qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/strided_slice└
Udrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/BroadcastTo/shapePackZdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/strided_slice:output:0*
N*
T0*
_output_shapes
:2W
Udrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/BroadcastTo/shapeН
Odrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/BroadcastToBroadcastToLdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Cast:y:0^drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/BroadcastTo/shape:output:0*
T0*#
_output_shapes
:         2Q
Odrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/BroadcastToр
Idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2K
Idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Constш
Gdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/MaxMaxXdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/BroadcastTo:output:0Rdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Const:output:0*
T0*
_output_shapes
: 2I
Gdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Maxр
Mdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Maximum/xConst*
_output_shapes
: *
dtype0*
value	B : 2O
Mdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Maximum/xЁ
Kdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/MaximumMaximumVdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Maximum/x:output:0Pdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Max:output:0*
T0*
_output_shapes
: 2M
Kdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/MaximumЄ
Vdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/ConstConst*
_output_shapes
: *
dtype0*
value	B : 2X
Vdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/ConstЎ
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/Const_1Const*
_output_shapes
: *
dtype0*
value	B :2Z
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/Const_1є
Vdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/RangeRange_drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/Const:output:0Odrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Maximum:z:0adrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/Const_1:output:0*#
_output_shapes
:         2X
Vdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/RangeН
_drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2a
_drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/ExpandDims/dim╛
[drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/ExpandDims
ExpandDimsXdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/BroadcastTo:output:0hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2]
[drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/ExpandDims▌
Udrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/CastCastddrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/ExpandDims:output:0*

DstT0*

SrcT0*'
_output_shapes
:         2W
Udrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/Castн
Udrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/LessLess_drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/Range:output:0Ydrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/Cast:y:0*
T0*0
_output_shapes
:                  2W
Udrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/Lessъ
Rdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :2T
Rdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/ExpandDims/dimК
Ndrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/ExpandDims
ExpandDimsKdrift/graph_network/global_block/nodes_to_globals_aggregator/range:output:0[drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2P
Ndrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/ExpandDimsю
Tdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Tile/multiples/0Const*
_output_shapes
: *
dtype0*
value	B :2V
Tdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Tile/multiples/0О
Rdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Tile/multiplesPack]drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Tile/multiples/0:output:0Odrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Maximum:z:0*
N*
T0*
_output_shapes
:2T
Rdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Tile/multiplesН
Hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/TileTileWdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/ExpandDims:output:0[drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Tile/multiples:output:0*
T0*0
_output_shapes
:                  2J
Hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Tile▒
Vdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/ShapeShapeQdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Tile:output:0*
T0*
_output_shapes
:2X
Vdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/ShapeЦ
ddrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2f
ddrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice/stackЪ
fdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2h
fdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack_1Ъ
fdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2h
fdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack_2┤
^drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_sliceStridedSlice_drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Shape:output:0mdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack:output:0odrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack_1:output:0odrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:2`
^drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_sliceЬ
gdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Prod/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2i
gdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Prod/reduction_indices▓
Udrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/ProdProdgdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice:output:0pdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Prod/reduction_indices:output:0*
T0*
_output_shapes
: 2W
Udrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Prod╡
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Shape_1ShapeQdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Tile:output:0*
T0*
_output_shapes
:2Z
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Shape_1Ъ
fdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2h
fdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stackЮ
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2j
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack_1Ю
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2j
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack_2╨
`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_1StridedSliceadrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Shape_1:output:0odrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack:output:0qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack_1:output:0qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *

begin_mask2b
`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_1╡
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Shape_2ShapeQdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Tile:output:0*
T0*
_output_shapes
:2Z
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Shape_2Ъ
fdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2h
fdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stackЮ
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2j
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack_1Ю
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2j
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack_2╬
`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_2StridedSliceadrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Shape_2:output:0odrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack:output:0qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack_1:output:0qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
end_mask2b
`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_2┌
`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/concat/values_1Pack^drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Prod:output:0*
N*
T0*
_output_shapes
:2b
`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/concat/values_1■
\drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2^
\drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/concat/axisФ
Wdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/concatConcatV2idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_1:output:0idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/concat/values_1:output:0idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/strided_slice_2:output:0edrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/concat/axis:output:0*
N*
T0*
_output_shapes
:2Y
Wdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/concatв
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/ReshapeReshapeQdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/Tile:output:0`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/concat:output:0*
T0*#
_output_shapes
:         2Z
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/ReshapeЧ
`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:
         2b
`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Reshape_1/shape╖
Zdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Reshape_1ReshapeYdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/SequenceMask/Less:z:0idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Reshape_1/shape:output:0*
T0
*#
_output_shapes
:         2\
Zdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Reshape_1╟
Vdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/WhereWherecdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Reshape_1:output:0*'
_output_shapes
:         2X
Vdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Whereф
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/SqueezeSqueeze^drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Where:index:0*
T0	*#
_output_shapes
:         *
squeeze_dims
2Z
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/SqueezeВ
^drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2`
^drift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/GatherV2/axis┬
Ydrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/GatherV2GatherV2adrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Reshape:output:0adrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/Squeeze:output:0gdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/GatherV2/axis:output:0*
Taxis0*
Tindices0	*
Tparams0*#
_output_shapes
:         2[
Ydrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/GatherV2ё
Odrift/graph_network/global_block/nodes_to_globals_aggregator/UnsortedSegmentSumUnsortedSegmentSum-drift/graph_network/node_block/linear/Add:z:0bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat/boolean_mask/GatherV2:output:0bdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs/strided_slice:output:0*
T0*
Tindices0*'
_output_shapes
:          2Q
Odrift/graph_network/global_block/nodes_to_globals_aggregator/UnsortedSegmentSumз
,drift/graph_network/global_block/concat/axisConst*
_output_shapes
: *
dtype0*
valueB :
         2.
,drift/graph_network/global_block/concat/axisМ
'drift/graph_network/global_block/concatConcatV2Xdrift/graph_network/global_block/edges_to_globals_aggregator/UnsortedSegmentSum:output:0Xdrift/graph_network/global_block/nodes_to_globals_aggregator/UnsortedSegmentSum:output:0comp_45drift/graph_network/global_block/concat/axis:output:0*
N*
T0*'
_output_shapes
:         @2)
'drift/graph_network/global_block/concatЕ
=drift/graph_network/global_block/linear/MatMul/ReadVariableOpReadVariableOpFdrift_graph_network_global_block_linear_matmul_readvariableop_resource*
_output_shapes

:@*
dtype02?
=drift/graph_network/global_block/linear/MatMul/ReadVariableOpХ
.drift/graph_network/global_block/linear/MatMulMatMul0drift/graph_network/global_block/concat:output:0Edrift/graph_network/global_block/linear/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         20
.drift/graph_network/global_block/linear/MatMul°
:drift/graph_network/global_block/linear/Add/ReadVariableOpReadVariableOpCdrift_graph_network_global_block_linear_add_readvariableop_resource*
_output_shapes
:*
dtype02<
:drift/graph_network/global_block/linear/Add/ReadVariableOpС
+drift/graph_network/global_block/linear/AddAdd8drift/graph_network/global_block/linear/MatMul:product:0Bdrift/graph_network/global_block/linear/Add/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2-
+drift/graph_network/global_block/linear/Addц
Pdrift/graph_network/edge_block/broadcast_receiver_nodes_to_edges_1/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2R
Pdrift/graph_network/edge_block/broadcast_receiver_nodes_to_edges_1/GatherV2/axisН
Kdrift/graph_network/edge_block/broadcast_receiver_nodes_to_edges_1/GatherV2GatherV2-drift/graph_network/node_block/linear/Add:z:0comp_2Ydrift/graph_network/edge_block/broadcast_receiver_nodes_to_edges_1/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*'
_output_shapes
:          2M
Kdrift/graph_network/edge_block/broadcast_receiver_nodes_to_edges_1/GatherV2т
Ndrift/graph_network/edge_block/broadcast_sender_nodes_to_edges_1/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2P
Ndrift/graph_network/edge_block/broadcast_sender_nodes_to_edges_1/GatherV2/axisЗ
Idrift/graph_network/edge_block/broadcast_sender_nodes_to_edges_1/GatherV2GatherV2-drift/graph_network/node_block/linear/Add:z:0comp_3Wdrift/graph_network/edge_block/broadcast_sender_nodes_to_edges_1/GatherV2/axis:output:0*
Taxis0*
Tindices0*
Tparams0*'
_output_shapes
:          2K
Idrift/graph_network/edge_block/broadcast_sender_nodes_to_edges_1/GatherV2▀
Gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/CastCastcomp_6*

DstT0*

SrcT0*#
_output_shapes
:         2I
Gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Castє
Hdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/ShapeShape/drift/graph_network/global_block/linear/Add:z:0*
T0*
_output_shapes
:2J
Hdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Shape·
Vdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2X
Vdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/strided_slice/stack■
Xdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2Z
Xdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/strided_slice/stack_1■
Xdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2Z
Xdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/strided_slice/stack_2Ї
Pdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/strided_sliceStridedSliceQdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Shape:output:0_drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/strided_slice/stack:output:0adrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/strided_slice/stack_1:output:0adrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2R
Pdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/strided_slice╜
Tdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/BroadcastTo/shapePackYdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/strided_slice:output:0*
N*
T0*
_output_shapes
:2V
Tdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/BroadcastTo/shapeЙ
Ndrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/BroadcastToBroadcastToKdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Cast:y:0]drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/BroadcastTo/shape:output:0*
T0*#
_output_shapes
:         2P
Ndrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/BroadcastTo▐
Hdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2J
Hdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Constф
Fdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/MaxMaxWdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/BroadcastTo:output:0Qdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Const:output:0*
T0*
_output_shapes
: 2H
Fdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Max▐
Ldrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Maximum/xConst*
_output_shapes
: *
dtype0*
value	B : 2N
Ldrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Maximum/xь
Jdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/MaximumMaximumUdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Maximum/x:output:0Odrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Max:output:0*
T0*
_output_shapes
: 2L
Jdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/MaximumЁ
Udrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/ConstConst*
_output_shapes
: *
dtype0*
value	B : 2W
Udrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/ConstЇ
Wdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/Const_1Const*
_output_shapes
: *
dtype0*
value	B :2Y
Wdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/Const_1ю
Udrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/RangeRange^drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/Const:output:0Ndrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Maximum:z:0`drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/Const_1:output:0*#
_output_shapes
:         2W
Udrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/RangeЛ
^drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2`
^drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/ExpandDims/dim║
Zdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/ExpandDims
ExpandDimsWdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/BroadcastTo:output:0gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2\
Zdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/ExpandDims┌
Tdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/CastCastcdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/ExpandDims:output:0*

DstT0*

SrcT0*'
_output_shapes
:         2V
Tdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/Castй
Tdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/LessLess^drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/Range:output:0Xdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/Cast:y:0*
T0*0
_output_shapes
:                  2V
Tdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/Lessш
Qdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :2S
Qdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/ExpandDims/dimя
Mdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/ExpandDims
ExpandDims/drift/graph_network/global_block/linear/Add:z:0Zdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/ExpandDims/dim:output:0*
T0*+
_output_shapes
:         2O
Mdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/ExpandDimsь
Sdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile/multiples/0Const*
_output_shapes
: *
dtype0*
value	B :2U
Sdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile/multiples/0ь
Sdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile/multiples/2Const*
_output_shapes
: *
dtype0*
value	B :2U
Sdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile/multiples/2ш
Qdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile/multiplesPack\drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile/multiples/0:output:0Ndrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Maximum:z:0\drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile/multiples/2:output:0*
N*
T0*
_output_shapes
:2S
Qdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile/multiplesН
Gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/TileTileVdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/ExpandDims:output:0Zdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile/multiples:output:0*
T0*4
_output_shapes"
 :                  2I
Gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tileо
Udrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/ShapeShapePdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile:output:0*
T0*
_output_shapes
:2W
Udrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/ShapeФ
cdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2e
cdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice/stackШ
edrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2g
edrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice/stack_1Ш
edrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2g
edrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice/stack_2о
]drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_sliceStridedSlice^drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Shape:output:0ldrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice/stack:output:0ndrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice/stack_1:output:0ndrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:2_
]drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_sliceЪ
fdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Prod/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2h
fdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Prod/reduction_indicesо
Tdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/ProdProdfdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice:output:0odrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Prod/reduction_indices:output:0*
T0*
_output_shapes
: 2V
Tdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Prod▓
Wdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Shape_1ShapePdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile:output:0*
T0*
_output_shapes
:2Y
Wdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Shape_1Ш
edrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2g
edrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_1/stackЬ
gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2i
gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_1/stack_1Ь
gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2i
gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_1/stack_2╩
_drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_1StridedSlice`drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Shape_1:output:0ndrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_1/stack:output:0pdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_1/stack_1:output:0pdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *

begin_mask2a
_drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_1▓
Wdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Shape_2ShapePdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile:output:0*
T0*
_output_shapes
:2Y
Wdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Shape_2Ш
edrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2g
edrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_2/stackЬ
gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2i
gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_2/stack_1Ь
gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2i
gdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_2/stack_2╩
_drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_2StridedSlice`drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Shape_2:output:0ndrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_2/stack:output:0pdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_2/stack_1:output:0pdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
:*
end_mask2a
_drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_2╫
_drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/concat/values_1Pack]drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Prod:output:0*
N*
T0*
_output_shapes
:2a
_drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/concat/values_1№
[drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2]
[drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/concat/axisО
Vdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/concatConcatV2hdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_1:output:0hdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/concat/values_1:output:0hdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/strided_slice_2:output:0ddrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/concat/axis:output:0*
N*
T0*
_output_shapes
:2X
Vdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/concatв
Wdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/ReshapeReshapePdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/Tile:output:0_drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/concat:output:0*
T0*'
_output_shapes
:         2Y
Wdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/ReshapeХ
_drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:
         2a
_drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Reshape_1/shape│
Ydrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Reshape_1ReshapeXdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/SequenceMask/Less:z:0hdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Reshape_1/shape:output:0*
T0
*#
_output_shapes
:         2[
Ydrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Reshape_1─
Udrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/WhereWherebdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Reshape_1:output:0*'
_output_shapes
:         2W
Udrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Whereс
Wdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/SqueezeSqueeze]drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Where:index:0*
T0	*#
_output_shapes
:         *
squeeze_dims
2Y
Wdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/SqueezeА
]drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2_
]drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/GatherV2/axis┴
Xdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/GatherV2GatherV2`drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Reshape:output:0`drift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/Squeeze:output:0fdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/GatherV2/axis:output:0*
Taxis0*
Tindices0	*
Tparams0*'
_output_shapes
:         2Z
Xdrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/GatherV2з
,drift/graph_network/edge_block/concat_1/axisConst*
_output_shapes
: *
dtype0*
valueB :
         2.
,drift/graph_network/edge_block/concat_1/axisМ
'drift/graph_network/edge_block/concat_1ConcatV2-drift/graph_network/edge_block/linear/Add:z:0Tdrift/graph_network/edge_block/broadcast_receiver_nodes_to_edges_1/GatherV2:output:0Rdrift/graph_network/edge_block/broadcast_sender_nodes_to_edges_1/GatherV2:output:0adrift/graph_network/edge_block/broadcast_globals_to_edges_1/repeat/boolean_mask/GatherV2:output:05drift/graph_network/edge_block/concat_1/axis:output:0*
N*
T0*'
_output_shapes
:         a2)
'drift/graph_network/edge_block/concat_1Е
=drift/graph_network/edge_block/linear/MatMul_1/ReadVariableOpReadVariableOpFdrift_graph_network_edge_block_linear_matmul_1_readvariableop_resource*
_output_shapes

:a *
dtype02?
=drift/graph_network/edge_block/linear/MatMul_1/ReadVariableOpХ
.drift/graph_network/edge_block/linear/MatMul_1MatMul0drift/graph_network/edge_block/concat_1:output:0Edrift/graph_network/edge_block/linear/MatMul_1/ReadVariableOp:value:0*
T0*'
_output_shapes
:          20
.drift/graph_network/edge_block/linear/MatMul_1°
:drift/graph_network/edge_block/linear/Add_1/ReadVariableOpReadVariableOpCdrift_graph_network_edge_block_linear_add_1_readvariableop_resource*
_output_shapes
: *
dtype02<
:drift/graph_network/edge_block/linear/Add_1/ReadVariableOpС
+drift/graph_network/edge_block/linear/Add_1Add8drift/graph_network/edge_block/linear/MatMul_1:product:0Bdrift/graph_network/edge_block/linear/Add_1/ReadVariableOp:value:0*
T0*'
_output_shapes
:          2-
+drift/graph_network/edge_block/linear/Add_1р
Idrift/graph_network/node_block/received_edges_to_nodes_aggregator/Const_1Const*
_output_shapes
:*
dtype0*
valueB: 2K
Idrift/graph_network/node_block/received_edges_to_nodes_aggregator/Const_1Ц
Gdrift/graph_network/node_block/received_edges_to_nodes_aggregator/Sum_1Sumcomp_5Rdrift/graph_network/node_block/received_edges_to_nodes_aggregator/Const_1:output:0*
T0*
_output_shapes
: 2I
Gdrift/graph_network/node_block/received_edges_to_nodes_aggregator/Sum_1У
Vdrift/graph_network/node_block/received_edges_to_nodes_aggregator/UnsortedSegmentSum_1UnsortedSegmentSum/drift/graph_network/edge_block/linear/Add_1:z:0comp_2Pdrift/graph_network/node_block/received_edges_to_nodes_aggregator/Sum_1:output:0*
T0*
Tindices0*'
_output_shapes
:          2X
Vdrift/graph_network/node_block/received_edges_to_nodes_aggregator/UnsortedSegmentSum_1▀
Gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/CastCastcomp_5*

DstT0*

SrcT0*#
_output_shapes
:         2I
Gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Castє
Hdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/ShapeShape/drift/graph_network/global_block/linear/Add:z:0*
T0*
_output_shapes
:2J
Hdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Shape·
Vdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2X
Vdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/strided_slice/stack■
Xdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2Z
Xdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/strided_slice/stack_1■
Xdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2Z
Xdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/strided_slice/stack_2Ї
Pdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/strided_sliceStridedSliceQdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Shape:output:0_drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/strided_slice/stack:output:0adrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/strided_slice/stack_1:output:0adrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2R
Pdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/strided_slice╜
Tdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/BroadcastTo/shapePackYdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/strided_slice:output:0*
N*
T0*
_output_shapes
:2V
Tdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/BroadcastTo/shapeЙ
Ndrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/BroadcastToBroadcastToKdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Cast:y:0]drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/BroadcastTo/shape:output:0*
T0*#
_output_shapes
:         2P
Ndrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/BroadcastTo▐
Hdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2J
Hdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Constф
Fdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/MaxMaxWdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/BroadcastTo:output:0Qdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Const:output:0*
T0*
_output_shapes
: 2H
Fdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Max▐
Ldrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Maximum/xConst*
_output_shapes
: *
dtype0*
value	B : 2N
Ldrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Maximum/xь
Jdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/MaximumMaximumUdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Maximum/x:output:0Odrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Max:output:0*
T0*
_output_shapes
: 2L
Jdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/MaximumЁ
Udrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/ConstConst*
_output_shapes
: *
dtype0*
value	B : 2W
Udrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/ConstЇ
Wdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/Const_1Const*
_output_shapes
: *
dtype0*
value	B :2Y
Wdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/Const_1ю
Udrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/RangeRange^drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/Const:output:0Ndrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Maximum:z:0`drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/Const_1:output:0*#
_output_shapes
:         2W
Udrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/RangeЛ
^drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2`
^drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/ExpandDims/dim║
Zdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/ExpandDims
ExpandDimsWdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/BroadcastTo:output:0gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2\
Zdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/ExpandDims┌
Tdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/CastCastcdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/ExpandDims:output:0*

DstT0*

SrcT0*'
_output_shapes
:         2V
Tdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/Castй
Tdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/LessLess^drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/Range:output:0Xdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/Cast:y:0*
T0*0
_output_shapes
:                  2V
Tdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/Lessш
Qdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :2S
Qdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/ExpandDims/dimя
Mdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/ExpandDims
ExpandDims/drift/graph_network/global_block/linear/Add:z:0Zdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/ExpandDims/dim:output:0*
T0*+
_output_shapes
:         2O
Mdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/ExpandDimsь
Sdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile/multiples/0Const*
_output_shapes
: *
dtype0*
value	B :2U
Sdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile/multiples/0ь
Sdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile/multiples/2Const*
_output_shapes
: *
dtype0*
value	B :2U
Sdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile/multiples/2ш
Qdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile/multiplesPack\drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile/multiples/0:output:0Ndrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Maximum:z:0\drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile/multiples/2:output:0*
N*
T0*
_output_shapes
:2S
Qdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile/multiplesН
Gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/TileTileVdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/ExpandDims:output:0Zdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile/multiples:output:0*
T0*4
_output_shapes"
 :                  2I
Gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tileо
Udrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/ShapeShapePdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile:output:0*
T0*
_output_shapes
:2W
Udrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/ShapeФ
cdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2e
cdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice/stackШ
edrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2g
edrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice/stack_1Ш
edrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2g
edrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice/stack_2о
]drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_sliceStridedSlice^drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Shape:output:0ldrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice/stack:output:0ndrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice/stack_1:output:0ndrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:2_
]drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_sliceЪ
fdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Prod/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2h
fdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Prod/reduction_indicesо
Tdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/ProdProdfdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice:output:0odrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Prod/reduction_indices:output:0*
T0*
_output_shapes
: 2V
Tdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Prod▓
Wdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Shape_1ShapePdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile:output:0*
T0*
_output_shapes
:2Y
Wdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Shape_1Ш
edrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2g
edrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_1/stackЬ
gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2i
gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_1/stack_1Ь
gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2i
gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_1/stack_2╩
_drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_1StridedSlice`drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Shape_1:output:0ndrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_1/stack:output:0pdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_1/stack_1:output:0pdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *

begin_mask2a
_drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_1▓
Wdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Shape_2ShapePdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile:output:0*
T0*
_output_shapes
:2Y
Wdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Shape_2Ш
edrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2g
edrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_2/stackЬ
gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2i
gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_2/stack_1Ь
gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2i
gdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_2/stack_2╩
_drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_2StridedSlice`drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Shape_2:output:0ndrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_2/stack:output:0pdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_2/stack_1:output:0pdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
:*
end_mask2a
_drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_2╫
_drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/concat/values_1Pack]drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Prod:output:0*
N*
T0*
_output_shapes
:2a
_drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/concat/values_1№
[drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2]
[drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/concat/axisО
Vdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/concatConcatV2hdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_1:output:0hdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/concat/values_1:output:0hdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/strided_slice_2:output:0ddrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/concat/axis:output:0*
N*
T0*
_output_shapes
:2X
Vdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/concatв
Wdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/ReshapeReshapePdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/Tile:output:0_drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/concat:output:0*
T0*'
_output_shapes
:         2Y
Wdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/ReshapeХ
_drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:
         2a
_drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Reshape_1/shape│
Ydrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Reshape_1ReshapeXdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/SequenceMask/Less:z:0hdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Reshape_1/shape:output:0*
T0
*#
_output_shapes
:         2[
Ydrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Reshape_1─
Udrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/WhereWherebdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Reshape_1:output:0*'
_output_shapes
:         2W
Udrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Whereс
Wdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/SqueezeSqueeze]drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Where:index:0*
T0	*#
_output_shapes
:         *
squeeze_dims
2Y
Wdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/SqueezeА
]drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2_
]drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/GatherV2/axis┴
Xdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/GatherV2GatherV2`drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Reshape:output:0`drift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/Squeeze:output:0fdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/GatherV2/axis:output:0*
Taxis0*
Tindices0	*
Tparams0*'
_output_shapes
:         2Z
Xdrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/GatherV2з
,drift/graph_network/node_block/concat_1/axisConst*
_output_shapes
: *
dtype0*
valueB :
         2.
,drift/graph_network/node_block/concat_1/axis├
'drift/graph_network/node_block/concat_1ConcatV2_drift/graph_network/node_block/received_edges_to_nodes_aggregator/UnsortedSegmentSum_1:output:0-drift/graph_network/node_block/linear/Add:z:0adrift/graph_network/node_block/broadcast_globals_to_nodes_1/repeat/boolean_mask/GatherV2:output:05drift/graph_network/node_block/concat_1/axis:output:0*
N*
T0*'
_output_shapes
:         A2)
'drift/graph_network/node_block/concat_1Е
=drift/graph_network/node_block/linear/MatMul_1/ReadVariableOpReadVariableOpFdrift_graph_network_node_block_linear_matmul_1_readvariableop_resource*
_output_shapes

:A *
dtype02?
=drift/graph_network/node_block/linear/MatMul_1/ReadVariableOpХ
.drift/graph_network/node_block/linear/MatMul_1MatMul0drift/graph_network/node_block/concat_1:output:0Edrift/graph_network/node_block/linear/MatMul_1/ReadVariableOp:value:0*
T0*'
_output_shapes
:          20
.drift/graph_network/node_block/linear/MatMul_1°
:drift/graph_network/node_block/linear/Add_1/ReadVariableOpReadVariableOpCdrift_graph_network_node_block_linear_add_1_readvariableop_resource*
_output_shapes
: *
dtype02<
:drift/graph_network/node_block/linear/Add_1/ReadVariableOpС
+drift/graph_network/node_block/linear/Add_1Add8drift/graph_network/node_block/linear/MatMul_1:product:0Bdrift/graph_network/node_block/linear/Add_1/ReadVariableOp:value:0*
T0*'
_output_shapes
:          2-
+drift/graph_network/node_block/linear/Add_1р
Sdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/ShapeShapecomp_5*
T0*
_output_shapes
:2U
Sdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/ShapeР
adrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2c
adrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_slice/stackФ
cdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2e
cdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_slice/stack_1Ф
cdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2e
cdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_slice/stack_2╢
[drift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_sliceStridedSlice\drift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/Shape:output:0jdrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_slice/stack:output:0ldrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_slice/stack_1:output:0ldrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2]
[drift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_slice┌
Jdrift/graph_network/global_block/edges_to_globals_aggregator/range_1/startConst*
_output_shapes
: *
dtype0*
value	B : 2L
Jdrift/graph_network/global_block/edges_to_globals_aggregator/range_1/start┌
Jdrift/graph_network/global_block/edges_to_globals_aggregator/range_1/deltaConst*
_output_shapes
: *
dtype0*
value	B :2L
Jdrift/graph_network/global_block/edges_to_globals_aggregator/range_1/delta╩
Ddrift/graph_network/global_block/edges_to_globals_aggregator/range_1RangeSdrift/graph_network/global_block/edges_to_globals_aggregator/range_1/start:output:0ddrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_slice:output:0Sdrift/graph_network/global_block/edges_to_globals_aggregator/range_1/delta:output:0*#
_output_shapes
:         2F
Ddrift/graph_network/global_block/edges_to_globals_aggregator/range_1х
Jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/CastCastcomp_6*

DstT0*

SrcT0*#
_output_shapes
:         2L
Jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/CastЧ
Kdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/ShapeShapeMdrift/graph_network/global_block/edges_to_globals_aggregator/range_1:output:0*
T0*
_output_shapes
:2M
Kdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/ShapeА
Ydrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2[
Ydrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/strided_slice/stackД
[drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2]
[drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/strided_slice/stack_1Д
[drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2]
[drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/strided_slice/stack_2Ж
Sdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/strided_sliceStridedSliceTdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Shape:output:0bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/strided_slice/stack:output:0ddrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/strided_slice/stack_1:output:0ddrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2U
Sdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/strided_slice╞
Wdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/BroadcastTo/shapePack\drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/strided_slice:output:0*
N*
T0*
_output_shapes
:2Y
Wdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/BroadcastTo/shapeХ
Qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/BroadcastToBroadcastToNdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Cast:y:0`drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/BroadcastTo/shape:output:0*
T0*#
_output_shapes
:         2S
Qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/BroadcastToф
Kdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2M
Kdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/ConstЁ
Idrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/MaxMaxZdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/BroadcastTo:output:0Tdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Const:output:0*
T0*
_output_shapes
: 2K
Idrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Maxф
Odrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Maximum/xConst*
_output_shapes
: *
dtype0*
value	B : 2Q
Odrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Maximum/x°
Mdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/MaximumMaximumXdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Maximum/x:output:0Rdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Max:output:0*
T0*
_output_shapes
: 2O
Mdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/MaximumЎ
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/ConstConst*
_output_shapes
: *
dtype0*
value	B : 2Z
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/Const·
Zdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/Const_1Const*
_output_shapes
: *
dtype0*
value	B :2\
Zdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/Const_1¤
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/RangeRangeadrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/Const:output:0Qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Maximum:z:0cdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/Const_1:output:0*#
_output_shapes
:         2Z
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/RangeС
adrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2c
adrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/ExpandDims/dim╞
]drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/ExpandDims
ExpandDimsZdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/BroadcastTo:output:0jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2_
]drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/ExpandDimsу
Wdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/CastCastfdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/ExpandDims:output:0*

DstT0*

SrcT0*'
_output_shapes
:         2Y
Wdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/Cast╡
Wdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/LessLessadrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/Range:output:0[drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/Cast:y:0*
T0*0
_output_shapes
:                  2Y
Wdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/Lessю
Tdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :2V
Tdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/ExpandDims/dimТ
Pdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/ExpandDims
ExpandDimsMdrift/graph_network/global_block/edges_to_globals_aggregator/range_1:output:0]drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2R
Pdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/ExpandDimsЄ
Vdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Tile/multiples/0Const*
_output_shapes
: *
dtype0*
value	B :2X
Vdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Tile/multiples/0Ц
Tdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Tile/multiplesPack_drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Tile/multiples/0:output:0Qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Maximum:z:0*
N*
T0*
_output_shapes
:2V
Tdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Tile/multiplesХ
Jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/TileTileYdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/ExpandDims:output:0]drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Tile/multiples:output:0*
T0*0
_output_shapes
:                  2L
Jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Tile╖
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/ShapeShapeSdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Tile:output:0*
T0*
_output_shapes
:2Z
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/ShapeЪ
fdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2h
fdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stackЮ
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2j
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack_1Ю
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2j
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack_2└
`drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_sliceStridedSliceadrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Shape:output:0odrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack:output:0qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack_1:output:0qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:2b
`drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_sliceа
idrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Prod/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2k
idrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Prod/reduction_indices║
Wdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/ProdProdidrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice:output:0rdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Prod/reduction_indices:output:0*
T0*
_output_shapes
: 2Y
Wdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Prod╗
Zdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Shape_1ShapeSdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Tile:output:0*
T0*
_output_shapes
:2\
Zdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Shape_1Ю
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2j
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stackв
jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2l
jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack_1в
jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2l
jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack_2▄
bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1StridedSlicecdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Shape_1:output:0qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack:output:0sdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack_1:output:0sdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *

begin_mask2d
bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1╗
Zdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Shape_2ShapeSdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Tile:output:0*
T0*
_output_shapes
:2\
Zdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Shape_2Ю
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2j
hdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stackв
jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2l
jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack_1в
jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2l
jdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack_2┌
bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2StridedSlicecdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Shape_2:output:0qdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack:output:0sdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack_1:output:0sdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
end_mask2d
bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2р
bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/concat/values_1Pack`drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Prod:output:0*
N*
T0*
_output_shapes
:2d
bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/concat/values_1В
^drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2`
^drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/concat/axisа
Ydrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/concatConcatV2kdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1:output:0kdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/concat/values_1:output:0kdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2:output:0gdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/concat/axis:output:0*
N*
T0*
_output_shapes
:2[
Ydrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/concatк
Zdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/ReshapeReshapeSdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/Tile:output:0bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/concat:output:0*
T0*#
_output_shapes
:         2\
Zdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/ReshapeЫ
bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:
         2d
bdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Reshape_1/shape┐
\drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Reshape_1Reshape[drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/SequenceMask/Less:z:0kdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Reshape_1/shape:output:0*
T0
*#
_output_shapes
:         2^
\drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Reshape_1═
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/WhereWhereedrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Reshape_1:output:0*'
_output_shapes
:         2Z
Xdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Whereъ
Zdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/SqueezeSqueeze`drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Where:index:0*
T0	*#
_output_shapes
:         *
squeeze_dims
2\
Zdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/SqueezeЖ
`drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2b
`drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/GatherV2/axis╠
[drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/GatherV2GatherV2cdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Reshape:output:0cdrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/Squeeze:output:0idrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/GatherV2/axis:output:0*
Taxis0*
Tindices0	*
Tparams0*#
_output_shapes
:         2]
[drift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/GatherV2√
Qdrift/graph_network/global_block/edges_to_globals_aggregator/UnsortedSegmentSum_1UnsortedSegmentSum/drift/graph_network/edge_block/linear/Add_1:z:0ddrift/graph_network/global_block/edges_to_globals_aggregator/repeat_1/boolean_mask/GatherV2:output:0ddrift/graph_network/global_block/edges_to_globals_aggregator/get_num_graphs_1/strided_slice:output:0*
T0*
Tindices0*'
_output_shapes
:          2S
Qdrift/graph_network/global_block/edges_to_globals_aggregator/UnsortedSegmentSum_1р
Sdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/ShapeShapecomp_5*
T0*
_output_shapes
:2U
Sdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/ShapeР
adrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2c
adrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_slice/stackФ
cdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2e
cdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_slice/stack_1Ф
cdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2e
cdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_slice/stack_2╢
[drift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_sliceStridedSlice\drift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/Shape:output:0jdrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_slice/stack:output:0ldrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_slice/stack_1:output:0ldrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2]
[drift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_slice┌
Jdrift/graph_network/global_block/nodes_to_globals_aggregator/range_1/startConst*
_output_shapes
: *
dtype0*
value	B : 2L
Jdrift/graph_network/global_block/nodes_to_globals_aggregator/range_1/start┌
Jdrift/graph_network/global_block/nodes_to_globals_aggregator/range_1/deltaConst*
_output_shapes
: *
dtype0*
value	B :2L
Jdrift/graph_network/global_block/nodes_to_globals_aggregator/range_1/delta╩
Ddrift/graph_network/global_block/nodes_to_globals_aggregator/range_1RangeSdrift/graph_network/global_block/nodes_to_globals_aggregator/range_1/start:output:0ddrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_slice:output:0Sdrift/graph_network/global_block/nodes_to_globals_aggregator/range_1/delta:output:0*#
_output_shapes
:         2F
Ddrift/graph_network/global_block/nodes_to_globals_aggregator/range_1х
Jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/CastCastcomp_5*

DstT0*

SrcT0*#
_output_shapes
:         2L
Jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/CastЧ
Kdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/ShapeShapeMdrift/graph_network/global_block/nodes_to_globals_aggregator/range_1:output:0*
T0*
_output_shapes
:2M
Kdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/ShapeА
Ydrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2[
Ydrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/strided_slice/stackД
[drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2]
[drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/strided_slice/stack_1Д
[drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2]
[drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/strided_slice/stack_2Ж
Sdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/strided_sliceStridedSliceTdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Shape:output:0bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/strided_slice/stack:output:0ddrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/strided_slice/stack_1:output:0ddrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2U
Sdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/strided_slice╞
Wdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/BroadcastTo/shapePack\drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/strided_slice:output:0*
N*
T0*
_output_shapes
:2Y
Wdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/BroadcastTo/shapeХ
Qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/BroadcastToBroadcastToNdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Cast:y:0`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/BroadcastTo/shape:output:0*
T0*#
_output_shapes
:         2S
Qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/BroadcastToф
Kdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2M
Kdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/ConstЁ
Idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/MaxMaxZdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/BroadcastTo:output:0Tdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Const:output:0*
T0*
_output_shapes
: 2K
Idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Maxф
Odrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Maximum/xConst*
_output_shapes
: *
dtype0*
value	B : 2Q
Odrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Maximum/x°
Mdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/MaximumMaximumXdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Maximum/x:output:0Rdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Max:output:0*
T0*
_output_shapes
: 2O
Mdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/MaximumЎ
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/ConstConst*
_output_shapes
: *
dtype0*
value	B : 2Z
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/Const·
Zdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/Const_1Const*
_output_shapes
: *
dtype0*
value	B :2\
Zdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/Const_1¤
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/RangeRangeadrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/Const:output:0Qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Maximum:z:0cdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/Const_1:output:0*#
_output_shapes
:         2Z
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/RangeС
adrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2c
adrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/ExpandDims/dim╞
]drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/ExpandDims
ExpandDimsZdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/BroadcastTo:output:0jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2_
]drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/ExpandDimsу
Wdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/CastCastfdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/ExpandDims:output:0*

DstT0*

SrcT0*'
_output_shapes
:         2Y
Wdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/Cast╡
Wdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/LessLessadrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/Range:output:0[drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/Cast:y:0*
T0*0
_output_shapes
:                  2Y
Wdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/Lessю
Tdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
value	B :2V
Tdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/ExpandDims/dimТ
Pdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/ExpandDims
ExpandDimsMdrift/graph_network/global_block/nodes_to_globals_aggregator/range_1:output:0]drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2R
Pdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/ExpandDimsЄ
Vdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Tile/multiples/0Const*
_output_shapes
: *
dtype0*
value	B :2X
Vdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Tile/multiples/0Ц
Tdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Tile/multiplesPack_drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Tile/multiples/0:output:0Qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Maximum:z:0*
N*
T0*
_output_shapes
:2V
Tdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Tile/multiplesХ
Jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/TileTileYdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/ExpandDims:output:0]drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Tile/multiples:output:0*
T0*0
_output_shapes
:                  2L
Jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Tile╖
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/ShapeShapeSdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Tile:output:0*
T0*
_output_shapes
:2Z
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/ShapeЪ
fdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2h
fdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stackЮ
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2j
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack_1Ю
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2j
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack_2└
`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_sliceStridedSliceadrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Shape:output:0odrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack:output:0qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack_1:output:0qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:2b
`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_sliceа
idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Prod/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2k
idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Prod/reduction_indices║
Wdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/ProdProdidrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice:output:0rdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Prod/reduction_indices:output:0*
T0*
_output_shapes
: 2Y
Wdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Prod╗
Zdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Shape_1ShapeSdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Tile:output:0*
T0*
_output_shapes
:2\
Zdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Shape_1Ю
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: 2j
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stackв
jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2l
jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack_1в
jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2l
jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack_2▄
bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1StridedSlicecdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Shape_1:output:0qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack:output:0sdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack_1:output:0sdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *

begin_mask2d
bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1╗
Zdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Shape_2ShapeSdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Tile:output:0*
T0*
_output_shapes
:2\
Zdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Shape_2Ю
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:2j
hdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stackв
jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: 2l
jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack_1в
jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2l
jdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack_2┌
bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2StridedSlicecdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Shape_2:output:0qdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack:output:0sdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack_1:output:0sdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
end_mask2d
bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2р
bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/concat/values_1Pack`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Prod:output:0*
N*
T0*
_output_shapes
:2d
bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/concat/values_1В
^drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2`
^drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/concat/axisа
Ydrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/concatConcatV2kdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_1:output:0kdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/concat/values_1:output:0kdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/strided_slice_2:output:0gdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/concat/axis:output:0*
N*
T0*
_output_shapes
:2[
Ydrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/concatк
Zdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/ReshapeReshapeSdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/Tile:output:0bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/concat:output:0*
T0*#
_output_shapes
:         2\
Zdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/ReshapeЫ
bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Reshape_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:
         2d
bdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Reshape_1/shape┐
\drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Reshape_1Reshape[drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/SequenceMask/Less:z:0kdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Reshape_1/shape:output:0*
T0
*#
_output_shapes
:         2^
\drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Reshape_1═
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/WhereWhereedrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Reshape_1:output:0*'
_output_shapes
:         2Z
Xdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Whereъ
Zdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/SqueezeSqueeze`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Where:index:0*
T0	*#
_output_shapes
:         *
squeeze_dims
2\
Zdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/SqueezeЖ
`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/GatherV2/axisConst*
_output_shapes
: *
dtype0*
value	B : 2b
`drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/GatherV2/axis╠
[drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/GatherV2GatherV2cdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Reshape:output:0cdrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/Squeeze:output:0idrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/GatherV2/axis:output:0*
Taxis0*
Tindices0	*
Tparams0*#
_output_shapes
:         2]
[drift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/GatherV2√
Qdrift/graph_network/global_block/nodes_to_globals_aggregator/UnsortedSegmentSum_1UnsortedSegmentSum/drift/graph_network/node_block/linear/Add_1:z:0ddrift/graph_network/global_block/nodes_to_globals_aggregator/repeat_1/boolean_mask/GatherV2:output:0ddrift/graph_network/global_block/nodes_to_globals_aggregator/get_num_graphs_1/strided_slice:output:0*
T0*
Tindices0*'
_output_shapes
:          2S
Qdrift/graph_network/global_block/nodes_to_globals_aggregator/UnsortedSegmentSum_1л
.drift/graph_network/global_block/concat_1/axisConst*
_output_shapes
: *
dtype0*
valueB :
         20
.drift/graph_network/global_block/concat_1/axis┐
)drift/graph_network/global_block/concat_1ConcatV2Zdrift/graph_network/global_block/edges_to_globals_aggregator/UnsortedSegmentSum_1:output:0Zdrift/graph_network/global_block/nodes_to_globals_aggregator/UnsortedSegmentSum_1:output:0/drift/graph_network/global_block/linear/Add:z:07drift/graph_network/global_block/concat_1/axis:output:0*
N*
T0*'
_output_shapes
:         A2+
)drift/graph_network/global_block/concat_1Л
?drift/graph_network/global_block/linear/MatMul_1/ReadVariableOpReadVariableOpHdrift_graph_network_global_block_linear_matmul_1_readvariableop_resource*
_output_shapes

:A*
dtype02A
?drift/graph_network/global_block/linear/MatMul_1/ReadVariableOpЭ
0drift/graph_network/global_block/linear/MatMul_1MatMul2drift/graph_network/global_block/concat_1:output:0Gdrift/graph_network/global_block/linear/MatMul_1/ReadVariableOp:value:0*
T0*'
_output_shapes
:         22
0drift/graph_network/global_block/linear/MatMul_1■
<drift/graph_network/global_block/linear/Add_1/ReadVariableOpReadVariableOpEdrift_graph_network_global_block_linear_add_1_readvariableop_resource*
_output_shapes
:*
dtype02>
<drift/graph_network/global_block/linear/Add_1/ReadVariableOpЩ
-drift/graph_network/global_block/linear/Add_1Add:drift/graph_network/global_block/linear/MatMul_1:product:0Ddrift/graph_network/global_block/linear/Add_1/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2/
-drift/graph_network/global_block/linear/Add_1
drift/Reshape/shapeConst*
_output_shapes
:*
dtype0*!
valueB"    @       2
drift/Reshape/shapeо
drift/ReshapeReshape/drift/graph_network/node_block/linear/Add_1:z:0drift/Reshape/shape:output:0*
T0*+
_output_shapes
:         @ 2
drift/Reshape┤
"drift/linear/MatMul/ReadVariableOpReadVariableOp+drift_linear_matmul_readvariableop_resource*
_output_shapes

: *
dtype02$
"drift/linear/MatMul/ReadVariableOpд
drift/linear/MatMulBatchMatMulV2prot*drift/linear/MatMul/ReadVariableOp:value:0*
T0*,
_output_shapes
:         А 2
drift/linear/MatMulз
drift/linear/Add/ReadVariableOpReadVariableOp(drift_linear_add_readvariableop_resource*
_output_shapes
: *
dtype02!
drift/linear/Add/ReadVariableOpй
drift/linear/AddAdddrift/linear/MatMul:output:0'drift/linear/Add/ReadVariableOp:value:0*
T0*,
_output_shapes
:         А 2
drift/linear/AddЯ
(drift/conv1_d/convolution/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
¤        2*
(drift/conv1_d/convolution/ExpandDims/dim▐
$drift/conv1_d/convolution/ExpandDims
ExpandDimsdrift/linear/Add:z:01drift/conv1_d/convolution/ExpandDims/dim:output:0*
T0*0
_output_shapes
:         А 2&
$drift/conv1_d/convolution/ExpandDimsё
5drift/conv1_d/convolution/ExpandDims_1/ReadVariableOpReadVariableOp>drift_conv1_d_convolution_expanddims_1_readvariableop_resource*"
_output_shapes
:  *
dtype027
5drift/conv1_d/convolution/ExpandDims_1/ReadVariableOpЪ
*drift/conv1_d/convolution/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : 2,
*drift/conv1_d/convolution/ExpandDims_1/dimГ
&drift/conv1_d/convolution/ExpandDims_1
ExpandDims=drift/conv1_d/convolution/ExpandDims_1/ReadVariableOp:value:03drift/conv1_d/convolution/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:  2(
&drift/conv1_d/convolution/ExpandDims_1Г
drift/conv1_d/convolutionConv2D-drift/conv1_d/convolution/ExpandDims:output:0/drift/conv1_d/convolution/ExpandDims_1:output:0*
T0*0
_output_shapes
:         А *
paddingSAME*
strides
2
drift/conv1_d/convolution╠
!drift/conv1_d/convolution/SqueezeSqueeze"drift/conv1_d/convolution:output:0*
T0*,
_output_shapes
:         А *
squeeze_dims

¤        2#
!drift/conv1_d/convolution/Squeeze╢
$drift/conv1_d/BiasAdd/ReadVariableOpReadVariableOp-drift_conv1_d_biasadd_readvariableop_resource*
_output_shapes
: *
dtype02&
$drift/conv1_d/BiasAdd/ReadVariableOp╩
drift/conv1_d/BiasAddBiasAdd*drift/conv1_d/convolution/Squeeze:output:0,drift/conv1_d/BiasAdd/ReadVariableOp:value:0*
T0*,
_output_shapes
:         А 2
drift/conv1_d/BiasAddг
*drift/conv1_d/convolution_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
¤        2,
*drift/conv1_d/convolution_1/ExpandDims/dimю
&drift/conv1_d/convolution_1/ExpandDims
ExpandDimsdrift/conv1_d/BiasAdd:output:03drift/conv1_d/convolution_1/ExpandDims/dim:output:0*
T0*0
_output_shapes
:         А 2(
&drift/conv1_d/convolution_1/ExpandDimsў
7drift/conv1_d/convolution_1/ExpandDims_1/ReadVariableOpReadVariableOp@drift_conv1_d_convolution_1_expanddims_1_readvariableop_resource*"
_output_shapes
:  *
dtype029
7drift/conv1_d/convolution_1/ExpandDims_1/ReadVariableOpЮ
,drift/conv1_d/convolution_1/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : 2.
,drift/conv1_d/convolution_1/ExpandDims_1/dimЛ
(drift/conv1_d/convolution_1/ExpandDims_1
ExpandDims?drift/conv1_d/convolution_1/ExpandDims_1/ReadVariableOp:value:05drift/conv1_d/convolution_1/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:  2*
(drift/conv1_d/convolution_1/ExpandDims_1Л
drift/conv1_d/convolution_1Conv2D/drift/conv1_d/convolution_1/ExpandDims:output:01drift/conv1_d/convolution_1/ExpandDims_1:output:0*
T0*0
_output_shapes
:         А *
paddingSAME*
strides
2
drift/conv1_d/convolution_1╥
#drift/conv1_d/convolution_1/SqueezeSqueeze$drift/conv1_d/convolution_1:output:0*
T0*,
_output_shapes
:         А *
squeeze_dims

¤        2%
#drift/conv1_d/convolution_1/Squeeze╝
&drift/conv1_d/BiasAdd_1/ReadVariableOpReadVariableOp/drift_conv1_d_biasadd_1_readvariableop_resource*
_output_shapes
: *
dtype02(
&drift/conv1_d/BiasAdd_1/ReadVariableOp╥
drift/conv1_d/BiasAdd_1BiasAdd,drift/conv1_d/convolution_1/Squeeze:output:0.drift/conv1_d/BiasAdd_1/ReadVariableOp:value:0*
T0*,
_output_shapes
:         А 2
drift/conv1_d/BiasAdd_1г
*drift/conv1_d/convolution_2/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
¤        2,
*drift/conv1_d/convolution_2/ExpandDims/dimЁ
&drift/conv1_d/convolution_2/ExpandDims
ExpandDims drift/conv1_d/BiasAdd_1:output:03drift/conv1_d/convolution_2/ExpandDims/dim:output:0*
T0*0
_output_shapes
:         А 2(
&drift/conv1_d/convolution_2/ExpandDimsў
7drift/conv1_d/convolution_2/ExpandDims_1/ReadVariableOpReadVariableOp@drift_conv1_d_convolution_2_expanddims_1_readvariableop_resource*"
_output_shapes
:  *
dtype029
7drift/conv1_d/convolution_2/ExpandDims_1/ReadVariableOpЮ
,drift/conv1_d/convolution_2/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : 2.
,drift/conv1_d/convolution_2/ExpandDims_1/dimЛ
(drift/conv1_d/convolution_2/ExpandDims_1
ExpandDims?drift/conv1_d/convolution_2/ExpandDims_1/ReadVariableOp:value:05drift/conv1_d/convolution_2/ExpandDims_1/dim:output:0*
T0*&
_output_shapes
:  2*
(drift/conv1_d/convolution_2/ExpandDims_1Л
drift/conv1_d/convolution_2Conv2D/drift/conv1_d/convolution_2/ExpandDims:output:01drift/conv1_d/convolution_2/ExpandDims_1:output:0*
T0*0
_output_shapes
:         А *
paddingSAME*
strides
2
drift/conv1_d/convolution_2╥
#drift/conv1_d/convolution_2/SqueezeSqueeze$drift/conv1_d/convolution_2:output:0*
T0*,
_output_shapes
:         А *
squeeze_dims

¤        2%
#drift/conv1_d/convolution_2/Squeeze╝
&drift/conv1_d/BiasAdd_2/ReadVariableOpReadVariableOp/drift_conv1_d_biasadd_2_readvariableop_resource*
_output_shapes
: *
dtype02(
&drift/conv1_d/BiasAdd_2/ReadVariableOp╥
drift/conv1_d/BiasAdd_2BiasAdd,drift/conv1_d/convolution_2/Squeeze:output:0.drift/conv1_d/BiasAdd_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:         А 2
drift/conv1_d/BiasAdd_2q
drift/concat/axisConst*
_output_shapes
: *
dtype0*
valueB :
         2
drift/concat/axis 
drift/concatConcatV2drift/linear/Add:z:0drift/conv1_d/BiasAdd:output:0 drift/conv1_d/BiasAdd_1:output:0 drift/conv1_d/BiasAdd_2:output:0drift/concat/axis:output:0*
N*
T0*-
_output_shapes
:         АА2
drift/concat╙
,drift/attention/linear/MatMul/ReadVariableOpReadVariableOp5drift_attention_linear_matmul_readvariableop_resource*
_output_shapes
:	 А*
dtype02.
,drift/attention/linear/MatMul/ReadVariableOp╘
drift/attention/linear/MatMulBatchMatMulV2drift/Reshape:output:04drift/attention/linear/MatMul/ReadVariableOp:value:0*
T0*,
_output_shapes
:         @А2
drift/attention/linear/MatMul╞
)drift/attention/linear/Add/ReadVariableOpReadVariableOp2drift_attention_linear_add_readvariableop_resource*
_output_shapes	
:А*
dtype02+
)drift/attention/linear/Add/ReadVariableOp╤
drift/attention/linear/AddAdd&drift/attention/linear/MatMul:output:01drift/attention/linear/Add/ReadVariableOp:value:0*
T0*,
_output_shapes
:         @А2
drift/attention/linear/AddС
drift/attention/LeakyRelu	LeakyReludrift/attention/linear/Add:z:0*,
_output_shapes
:         @А2
drift/attention/LeakyRelu┌
.drift/attention/linear/MatMul_1/ReadVariableOpReadVariableOp7drift_attention_linear_matmul_1_readvariableop_resource* 
_output_shapes
:
АА*
dtype020
.drift/attention/linear/MatMul_1/ReadVariableOp┌
drift/attention/linear/MatMul_1BatchMatMulV2drift/concat:output:06drift/attention/linear/MatMul_1/ReadVariableOp:value:0*
T0*-
_output_shapes
:         АА2!
drift/attention/linear/MatMul_1╠
+drift/attention/linear/Add_1/ReadVariableOpReadVariableOp4drift_attention_linear_add_1_readvariableop_resource*
_output_shapes	
:А*
dtype02-
+drift/attention/linear/Add_1/ReadVariableOp┌
drift/attention/linear/Add_1Add(drift/attention/linear/MatMul_1:output:03drift/attention/linear/Add_1/ReadVariableOp:value:0*
T0*-
_output_shapes
:         АА2
drift/attention/linear/Add_1Ш
drift/attention/LeakyRelu_1	LeakyRelu drift/attention/linear/Add_1:z:0*-
_output_shapes
:         АА2
drift/attention/LeakyRelu_1┌
.drift/attention/linear/MatMul_2/ReadVariableOpReadVariableOp7drift_attention_linear_matmul_2_readvariableop_resource* 
_output_shapes
:
АА*
dtype020
.drift/attention/linear/MatMul_2/ReadVariableOpы
drift/attention/linear/MatMul_2BatchMatMulV2'drift/attention/LeakyRelu:activations:06drift/attention/linear/MatMul_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:         @А2!
drift/attention/linear/MatMul_2╠
+drift/attention/linear/Add_2/ReadVariableOpReadVariableOp4drift_attention_linear_add_2_readvariableop_resource*
_output_shapes	
:А*
dtype02-
+drift/attention/linear/Add_2/ReadVariableOp┘
drift/attention/linear/Add_2Add(drift/attention/linear/MatMul_2:output:03drift/attention/linear/Add_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:         @А2
drift/attention/linear/Add_2Ч
drift/attention/LeakyRelu_2	LeakyRelu drift/attention/linear/Add_2:z:0*,
_output_shapes
:         @А2
drift/attention/LeakyRelu_2┌
.drift/attention/linear/MatMul_3/ReadVariableOpReadVariableOp7drift_attention_linear_matmul_3_readvariableop_resource* 
_output_shapes
:
АА*
dtype020
.drift/attention/linear/MatMul_3/ReadVariableOpю
drift/attention/linear/MatMul_3BatchMatMulV2)drift/attention/LeakyRelu_1:activations:06drift/attention/linear/MatMul_3/ReadVariableOp:value:0*
T0*-
_output_shapes
:         АА2!
drift/attention/linear/MatMul_3╠
+drift/attention/linear/Add_3/ReadVariableOpReadVariableOp4drift_attention_linear_add_3_readvariableop_resource*
_output_shapes	
:А*
dtype02-
+drift/attention/linear/Add_3/ReadVariableOp┌
drift/attention/linear/Add_3Add(drift/attention/linear/MatMul_3:output:03drift/attention/linear/Add_3/ReadVariableOp:value:0*
T0*-
_output_shapes
:         АА2
drift/attention/linear/Add_3Ш
drift/attention/LeakyRelu_3	LeakyRelu drift/attention/linear/Add_3:z:0*-
_output_shapes
:         АА2
drift/attention/LeakyRelu_3┌
.drift/attention/linear/MatMul_4/ReadVariableOpReadVariableOp7drift_attention_linear_matmul_4_readvariableop_resource* 
_output_shapes
:
АА*
dtype020
.drift/attention/linear/MatMul_4/ReadVariableOpэ
drift/attention/linear/MatMul_4BatchMatMulV2)drift/attention/LeakyRelu_2:activations:06drift/attention/linear/MatMul_4/ReadVariableOp:value:0*
T0*,
_output_shapes
:         @А2!
drift/attention/linear/MatMul_4╠
+drift/attention/linear/Add_4/ReadVariableOpReadVariableOp4drift_attention_linear_add_4_readvariableop_resource*
_output_shapes	
:А*
dtype02-
+drift/attention/linear/Add_4/ReadVariableOp┘
drift/attention/linear/Add_4Add(drift/attention/linear/MatMul_4:output:03drift/attention/linear/Add_4/ReadVariableOp:value:0*
T0*,
_output_shapes
:         @А2
drift/attention/linear/Add_4Ч
drift/attention/LeakyRelu_4	LeakyRelu drift/attention/linear/Add_4:z:0*,
_output_shapes
:         @А2
drift/attention/LeakyRelu_4┌
.drift/attention/linear/MatMul_5/ReadVariableOpReadVariableOp7drift_attention_linear_matmul_5_readvariableop_resource* 
_output_shapes
:
АА*
dtype020
.drift/attention/linear/MatMul_5/ReadVariableOpю
drift/attention/linear/MatMul_5BatchMatMulV2)drift/attention/LeakyRelu_3:activations:06drift/attention/linear/MatMul_5/ReadVariableOp:value:0*
T0*-
_output_shapes
:         АА2!
drift/attention/linear/MatMul_5╠
+drift/attention/linear/Add_5/ReadVariableOpReadVariableOp4drift_attention_linear_add_5_readvariableop_resource*
_output_shapes	
:А*
dtype02-
+drift/attention/linear/Add_5/ReadVariableOp┌
drift/attention/linear/Add_5Add(drift/attention/linear/MatMul_5:output:03drift/attention/linear/Add_5/ReadVariableOp:value:0*
T0*-
_output_shapes
:         АА2
drift/attention/linear/Add_5Ш
drift/attention/LeakyRelu_5	LeakyRelu drift/attention/linear/Add_5:z:0*-
_output_shapes
:         АА2
drift/attention/LeakyRelu_5┌
.drift/attention/linear/MatMul_6/ReadVariableOpReadVariableOp7drift_attention_linear_matmul_6_readvariableop_resource* 
_output_shapes
:
АА*
dtype020
.drift/attention/linear/MatMul_6/ReadVariableOpэ
drift/attention/linear/MatMul_6BatchMatMulV2)drift/attention/LeakyRelu_4:activations:06drift/attention/linear/MatMul_6/ReadVariableOp:value:0*
T0*,
_output_shapes
:         @А2!
drift/attention/linear/MatMul_6╠
+drift/attention/linear/Add_6/ReadVariableOpReadVariableOp4drift_attention_linear_add_6_readvariableop_resource*
_output_shapes	
:А*
dtype02-
+drift/attention/linear/Add_6/ReadVariableOp┘
drift/attention/linear/Add_6Add(drift/attention/linear/MatMul_6:output:03drift/attention/linear/Add_6/ReadVariableOp:value:0*
T0*,
_output_shapes
:         @А2
drift/attention/linear/Add_6Ч
drift/attention/LeakyRelu_6	LeakyRelu drift/attention/linear/Add_6:z:0*,
_output_shapes
:         @А2
drift/attention/LeakyRelu_6┌
.drift/attention/linear/MatMul_7/ReadVariableOpReadVariableOp7drift_attention_linear_matmul_7_readvariableop_resource* 
_output_shapes
:
АА*
dtype020
.drift/attention/linear/MatMul_7/ReadVariableOpю
drift/attention/linear/MatMul_7BatchMatMulV2)drift/attention/LeakyRelu_5:activations:06drift/attention/linear/MatMul_7/ReadVariableOp:value:0*
T0*-
_output_shapes
:         АА2!
drift/attention/linear/MatMul_7╠
+drift/attention/linear/Add_7/ReadVariableOpReadVariableOp4drift_attention_linear_add_7_readvariableop_resource*
_output_shapes	
:А*
dtype02-
+drift/attention/linear/Add_7/ReadVariableOp┌
drift/attention/linear/Add_7Add(drift/attention/linear/MatMul_7:output:03drift/attention/linear/Add_7/ReadVariableOp:value:0*
T0*-
_output_shapes
:         АА2
drift/attention/linear/Add_7Ш
drift/attention/LeakyRelu_7	LeakyRelu drift/attention/linear/Add_7:z:0*-
_output_shapes
:         АА2
drift/attention/LeakyRelu_7┌
.drift/attention/linear/MatMul_8/ReadVariableOpReadVariableOp7drift_attention_linear_matmul_8_readvariableop_resource* 
_output_shapes
:
АА*
dtype020
.drift/attention/linear/MatMul_8/ReadVariableOpэ
drift/attention/linear/MatMul_8BatchMatMulV2)drift/attention/LeakyRelu_6:activations:06drift/attention/linear/MatMul_8/ReadVariableOp:value:0*
T0*,
_output_shapes
:         @А2!
drift/attention/linear/MatMul_8╠
+drift/attention/linear/Add_8/ReadVariableOpReadVariableOp4drift_attention_linear_add_8_readvariableop_resource*
_output_shapes	
:А*
dtype02-
+drift/attention/linear/Add_8/ReadVariableOp┘
drift/attention/linear/Add_8Add(drift/attention/linear/MatMul_8:output:03drift/attention/linear/Add_8/ReadVariableOp:value:0*
T0*,
_output_shapes
:         @А2
drift/attention/linear/Add_8Ч
drift/attention/LeakyRelu_8	LeakyRelu drift/attention/linear/Add_8:z:0*,
_output_shapes
:         @А2
drift/attention/LeakyRelu_8┌
.drift/attention/linear/MatMul_9/ReadVariableOpReadVariableOp7drift_attention_linear_matmul_9_readvariableop_resource* 
_output_shapes
:
АА*
dtype020
.drift/attention/linear/MatMul_9/ReadVariableOpю
drift/attention/linear/MatMul_9BatchMatMulV2)drift/attention/LeakyRelu_7:activations:06drift/attention/linear/MatMul_9/ReadVariableOp:value:0*
T0*-
_output_shapes
:         АА2!
drift/attention/linear/MatMul_9╠
+drift/attention/linear/Add_9/ReadVariableOpReadVariableOp4drift_attention_linear_add_9_readvariableop_resource*
_output_shapes	
:А*
dtype02-
+drift/attention/linear/Add_9/ReadVariableOp┌
drift/attention/linear/Add_9Add(drift/attention/linear/MatMul_9:output:03drift/attention/linear/Add_9/ReadVariableOp:value:0*
T0*-
_output_shapes
:         АА2
drift/attention/linear/Add_9Ш
drift/attention/LeakyRelu_9	LeakyRelu drift/attention/linear/Add_9:z:0*-
_output_shapes
:         АА2
drift/attention/LeakyRelu_9▌
/drift/attention/linear/MatMul_10/ReadVariableOpReadVariableOp8drift_attention_linear_matmul_10_readvariableop_resource* 
_output_shapes
:
А└*
dtype021
/drift/attention/linear/MatMul_10/ReadVariableOpЁ
 drift/attention/linear/MatMul_10BatchMatMulV2)drift/attention/LeakyRelu_8:activations:07drift/attention/linear/MatMul_10/ReadVariableOp:value:0*
T0*,
_output_shapes
:         @└2"
 drift/attention/linear/MatMul_10▌
/drift/attention/linear/MatMul_11/ReadVariableOpReadVariableOp8drift_attention_linear_matmul_11_readvariableop_resource* 
_output_shapes
:
А└*
dtype021
/drift/attention/linear/MatMul_11/ReadVariableOpё
 drift/attention/linear/MatMul_11BatchMatMulV2)drift/attention/LeakyRelu_9:activations:07drift/attention/linear/MatMul_11/ReadVariableOp:value:0*
T0*-
_output_shapes
:         А└2"
 drift/attention/linear/MatMul_11Я
#drift/attention/strided_slice/stackConst*
_output_shapes
:*
dtype0*!
valueB"            2%
#drift/attention/strided_slice/stackг
%drift/attention/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*!
valueB"            2'
%drift/attention/strided_slice/stack_1г
%drift/attention/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*!
valueB"         2'
%drift/attention/strided_slice/stack_2с
drift/attention/strided_sliceStridedSlice	mask_comp,drift/attention/strided_slice/stack:output:0.drift/attention/strided_slice/stack_1:output:0.drift/attention/strided_slice/stack_2:output:0*
Index0*
T0*+
_output_shapes
:         @*

begin_mask*
end_mask*
new_axis_mask2
drift/attention/strided_sliceГ
drift/attention/ConstConst*
_output_shapes
:*
dtype0*!
valueB"      @  2
drift/attention/Const│
drift/attention/TileTile&drift/attention/strided_slice:output:0drift/attention/Const:output:0*
T0*,
_output_shapes
:         @└2
drift/attention/Tileг
%drift/attention/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*!
valueB"            2'
%drift/attention/strided_slice_1/stackз
'drift/attention/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*!
valueB"            2)
'drift/attention/strided_slice_1/stack_1з
'drift/attention/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*!
valueB"         2)
'drift/attention/strided_slice_1/stack_2ь
drift/attention/strided_slice_1StridedSlice	mask_prot.drift/attention/strided_slice_1/stack:output:00drift/attention/strided_slice_1/stack_1:output:00drift/attention/strided_slice_1/stack_2:output:0*
Index0*
T0*,
_output_shapes
:         А*

begin_mask*
end_mask*
new_axis_mask2!
drift/attention/strided_slice_1З
drift/attention/Const_1Const*
_output_shapes
:*
dtype0*!
valueB"      @  2
drift/attention/Const_1╝
drift/attention/Tile_1Tile(drift/attention/strided_slice_1:output:0 drift/attention/Const_1:output:0*
T0*-
_output_shapes
:         А└2
drift/attention/Tile_1▓
drift/attention/mulMul)drift/attention/linear/MatMul_10:output:0drift/attention/Tile:output:0*
T0*,
_output_shapes
:         @└2
drift/attention/mul╣
drift/attention/mul_1Mul)drift/attention/linear/MatMul_11:output:0drift/attention/Tile_1:output:0*
T0*-
_output_shapes
:         А└2
drift/attention/mul_1╣
drift/attention/MatMulBatchMatMulV2drift/attention/mul:z:0drift/attention/mul_1:z:0*
T0*,
_output_shapes
:         @А*
adj_y(2
drift/attention/MatMuli
drift/dropout/LogicalNot
LogicalNotis_training*
_output_shapes
:2
drift/dropout/LogicalNotД
drift/dropout/cond/SqueezeSqueezedrift/dropout/LogicalNot:y:0*
T0
*
_output_shapes
:2
drift/dropout/cond/Squeezeк
drift/dropout/condIf#drift/dropout/cond/Squeeze:output:0drift/attention/MatMul:output:0*
Tcond0
*
Tin
2*
Tout
2
*
_lower_using_switch_merge(*.
_output_shapes
:         @А: * 
_read_only_resource_inputs
 *3
else_branch$R"
 drift_dropout_cond_false_1126573*-
output_shapes
:         @А: *2
then_branch#R!
drift_dropout_cond_true_11265722
drift/dropout/condЪ
drift/dropout/cond/IdentityIdentitydrift/dropout/cond:output:0*
T0*,
_output_shapes
:         @А2
drift/dropout/cond/IdentityИ
drift/dropout/cond/Identity_1Identitydrift/dropout/cond:output:1*
T0
*
_output_shapes
: 2
drift/dropout/cond/Identity_1Е
drift/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
         2
drift/Max/reduction_indicesЫ
	drift/MaxMax$drift/dropout/cond/Identity:output:0$drift/Max/reduction_indices:output:0*
T0*'
_output_shapes
:         @2
	drift/MaxЕ
drift/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
         2
drift/Sum/reduction_indicesЕ
	drift/SumSumdrift/Max:output:0$drift/Sum/reduction_indices:output:0*
T0*#
_output_shapes
:         2
	drift/Sumw
IdentityIdentitydrift/Sum:output:0^drift/dropout/cond*
T0*#
_output_shapes
:         2

IdentityЦ

Identity_1Identity$drift/dropout/cond/Identity:output:0^drift/dropout/cond*
T0*,
_output_shapes
:         @А2

Identity_1"
identityIdentity:output:0"!

identity_1Identity_1:output:0*є
_input_shapesс
▐:         R:         :         :         :          :         :         :         А:         @:         А:::::::::::::::::::::::::::::::::::::::::::2(
drift/dropout/conddrift/dropout/cond:M I
'
_output_shapes
:         R

_user_specified_namecomp:MI
'
_output_shapes
:         

_user_specified_namecomp:IE
#
_output_shapes
:         

_user_specified_namecomp:IE
#
_output_shapes
:         

_user_specified_namecomp:KG
%
_output_shapes
:          

_user_specified_namecomp:IE
#
_output_shapes
:         

_user_specified_namecomp:IE
#
_output_shapes
:         

_user_specified_namecomp:RN
,
_output_shapes
:         А

_user_specified_nameprot:RN
'
_output_shapes
:         @
#
_user_specified_name	mask_comp:S	O
(
_output_shapes
:         А
#
_user_specified_name	mask_prot:E
A

_output_shapes
:
%
_user_specified_nameis_training
▌X
ў
 __inference__traced_save_1126868
file_prefix7
3savev2_drift_attention_linear_w_read_readvariableop7
3savev2_drift_attention_linear_b_read_readvariableop9
5savev2_drift_attention_linear_w_1_read_readvariableop9
5savev2_drift_attention_linear_b_1_read_readvariableop9
5savev2_drift_attention_linear_w_2_read_readvariableop9
5savev2_drift_attention_linear_b_2_read_readvariableop9
5savev2_drift_attention_linear_w_3_read_readvariableop9
5savev2_drift_attention_linear_b_3_read_readvariableop9
5savev2_drift_attention_linear_w_4_read_readvariableop9
5savev2_drift_attention_linear_b_4_read_readvariableop9
5savev2_drift_attention_linear_w_5_read_readvariableop9
5savev2_drift_attention_linear_w_6_read_readvariableop9
5savev2_drift_attention_linear_b_5_read_readvariableop9
5savev2_drift_attention_linear_w_7_read_readvariableop9
5savev2_drift_attention_linear_b_6_read_readvariableop9
5savev2_drift_attention_linear_w_8_read_readvariableop9
5savev2_drift_attention_linear_b_7_read_readvariableop9
5savev2_drift_attention_linear_w_9_read_readvariableop9
5savev2_drift_attention_linear_b_8_read_readvariableop:
6savev2_drift_attention_linear_w_10_read_readvariableop9
5savev2_drift_attention_linear_b_9_read_readvariableop:
6savev2_drift_attention_linear_w_11_read_readvariableopF
Bsavev2_drift_graph_network_edge_block_linear_b_read_readvariableopF
Bsavev2_drift_graph_network_edge_block_linear_w_read_readvariableopH
Dsavev2_drift_graph_network_global_block_linear_b_read_readvariableopH
Dsavev2_drift_graph_network_global_block_linear_w_read_readvariableopF
Bsavev2_drift_graph_network_node_block_linear_b_read_readvariableopF
Bsavev2_drift_graph_network_node_block_linear_w_read_readvariableopH
Dsavev2_drift_graph_network_edge_block_linear_b_1_read_readvariableopH
Dsavev2_drift_graph_network_edge_block_linear_w_1_read_readvariableopJ
Fsavev2_drift_graph_network_global_block_linear_b_1_read_readvariableopJ
Fsavev2_drift_graph_network_global_block_linear_w_1_read_readvariableopH
Dsavev2_drift_graph_network_node_block_linear_b_1_read_readvariableopH
Dsavev2_drift_graph_network_node_block_linear_w_1_read_readvariableop.
*savev2_drift_conv1_d_b_read_readvariableop.
*savev2_drift_conv1_d_w_read_readvariableop0
,savev2_drift_conv1_d_b_1_read_readvariableop0
,savev2_drift_conv1_d_w_1_read_readvariableop0
,savev2_drift_conv1_d_b_2_read_readvariableop0
,savev2_drift_conv1_d_w_2_read_readvariableop-
)savev2_drift_linear_b_read_readvariableop-
)savev2_drift_linear_w_read_readvariableop
savev2_const

identity_1ИвMergeV2CheckpointsП
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
ConstН
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*<
value3B1 B+_temp_a3d8d002abd944928fc32b008ae2488c/part2	
Const_1Л
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shardж
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename∙
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:+*
dtype0*Л
valueБB■+B*all_variables/0/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/1/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/2/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/3/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/4/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/5/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/6/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/7/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/8/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/9/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/10/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/11/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/12/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/13/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/14/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/15/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/16/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/17/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/18/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/19/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/20/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/21/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/22/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/23/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/24/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/25/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/26/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/27/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/28/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/29/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/30/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/31/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/32/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/33/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/34/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/35/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/36/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/37/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/38/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/39/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/40/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/41/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names▐
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:+*
dtype0*i
value`B^+B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices╞
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:03savev2_drift_attention_linear_w_read_readvariableop3savev2_drift_attention_linear_b_read_readvariableop5savev2_drift_attention_linear_w_1_read_readvariableop5savev2_drift_attention_linear_b_1_read_readvariableop5savev2_drift_attention_linear_w_2_read_readvariableop5savev2_drift_attention_linear_b_2_read_readvariableop5savev2_drift_attention_linear_w_3_read_readvariableop5savev2_drift_attention_linear_b_3_read_readvariableop5savev2_drift_attention_linear_w_4_read_readvariableop5savev2_drift_attention_linear_b_4_read_readvariableop5savev2_drift_attention_linear_w_5_read_readvariableop5savev2_drift_attention_linear_w_6_read_readvariableop5savev2_drift_attention_linear_b_5_read_readvariableop5savev2_drift_attention_linear_w_7_read_readvariableop5savev2_drift_attention_linear_b_6_read_readvariableop5savev2_drift_attention_linear_w_8_read_readvariableop5savev2_drift_attention_linear_b_7_read_readvariableop5savev2_drift_attention_linear_w_9_read_readvariableop5savev2_drift_attention_linear_b_8_read_readvariableop6savev2_drift_attention_linear_w_10_read_readvariableop5savev2_drift_attention_linear_b_9_read_readvariableop6savev2_drift_attention_linear_w_11_read_readvariableopBsavev2_drift_graph_network_edge_block_linear_b_read_readvariableopBsavev2_drift_graph_network_edge_block_linear_w_read_readvariableopDsavev2_drift_graph_network_global_block_linear_b_read_readvariableopDsavev2_drift_graph_network_global_block_linear_w_read_readvariableopBsavev2_drift_graph_network_node_block_linear_b_read_readvariableopBsavev2_drift_graph_network_node_block_linear_w_read_readvariableopDsavev2_drift_graph_network_edge_block_linear_b_1_read_readvariableopDsavev2_drift_graph_network_edge_block_linear_w_1_read_readvariableopFsavev2_drift_graph_network_global_block_linear_b_1_read_readvariableopFsavev2_drift_graph_network_global_block_linear_w_1_read_readvariableopDsavev2_drift_graph_network_node_block_linear_b_1_read_readvariableopDsavev2_drift_graph_network_node_block_linear_w_1_read_readvariableop*savev2_drift_conv1_d_b_read_readvariableop*savev2_drift_conv1_d_w_read_readvariableop,savev2_drift_conv1_d_b_1_read_readvariableop,savev2_drift_conv1_d_w_1_read_readvariableop,savev2_drift_conv1_d_b_2_read_readvariableop,savev2_drift_conv1_d_w_2_read_readvariableop)savev2_drift_linear_b_read_readvariableop)savev2_drift_linear_w_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *9
dtypes/
-2+2
SaveV2║
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixesб
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*Ы
_input_shapesЙ
Ж: :
А└:А:	 А:А:
АА:А:
АА:А:
АА:А:
АА:
А└:А:
АА:А:
АА:А:
АА:А:
АА:А:
АА: :	к ::@: :r : :a ::A: :A : :  : :  : :  : : : 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:&"
 
_output_shapes
:
А└:!

_output_shapes	
:А:%!

_output_shapes
:	 А:!

_output_shapes	
:А:&"
 
_output_shapes
:
АА:!

_output_shapes	
:А:&"
 
_output_shapes
:
АА:!

_output_shapes	
:А:&	"
 
_output_shapes
:
АА:!


_output_shapes	
:А:&"
 
_output_shapes
:
АА:&"
 
_output_shapes
:
А└:!

_output_shapes	
:А:&"
 
_output_shapes
:
АА:!

_output_shapes	
:А:&"
 
_output_shapes
:
АА:!

_output_shapes	
:А:&"
 
_output_shapes
:
АА:!

_output_shapes	
:А:&"
 
_output_shapes
:
АА:!

_output_shapes	
:А:&"
 
_output_shapes
:
АА: 

_output_shapes
: :%!

_output_shapes
:	к : 

_output_shapes
::$ 

_output_shapes

:@: 

_output_shapes
: :$ 

_output_shapes

:r : 

_output_shapes
: :$ 

_output_shapes

:a : 

_output_shapes
::$  

_output_shapes

:A: !

_output_shapes
: :$" 

_output_shapes

:A : #

_output_shapes
: :($$
"
_output_shapes
:  : %

_output_shapes
: :(&$
"
_output_shapes
:  : '

_output_shapes
: :(($
"
_output_shapes
:  : )

_output_shapes
: :$* 

_output_shapes

: :+

_output_shapes
: 
▐░
▓
#__inference__traced_restore_1127004
file_prefix-
)assignvariableop_drift_attention_linear_w/
+assignvariableop_1_drift_attention_linear_b1
-assignvariableop_2_drift_attention_linear_w_11
-assignvariableop_3_drift_attention_linear_b_11
-assignvariableop_4_drift_attention_linear_w_21
-assignvariableop_5_drift_attention_linear_b_21
-assignvariableop_6_drift_attention_linear_w_31
-assignvariableop_7_drift_attention_linear_b_31
-assignvariableop_8_drift_attention_linear_w_41
-assignvariableop_9_drift_attention_linear_b_42
.assignvariableop_10_drift_attention_linear_w_52
.assignvariableop_11_drift_attention_linear_w_62
.assignvariableop_12_drift_attention_linear_b_52
.assignvariableop_13_drift_attention_linear_w_72
.assignvariableop_14_drift_attention_linear_b_62
.assignvariableop_15_drift_attention_linear_w_82
.assignvariableop_16_drift_attention_linear_b_72
.assignvariableop_17_drift_attention_linear_w_92
.assignvariableop_18_drift_attention_linear_b_83
/assignvariableop_19_drift_attention_linear_w_102
.assignvariableop_20_drift_attention_linear_b_93
/assignvariableop_21_drift_attention_linear_w_11?
;assignvariableop_22_drift_graph_network_edge_block_linear_b?
;assignvariableop_23_drift_graph_network_edge_block_linear_wA
=assignvariableop_24_drift_graph_network_global_block_linear_bA
=assignvariableop_25_drift_graph_network_global_block_linear_w?
;assignvariableop_26_drift_graph_network_node_block_linear_b?
;assignvariableop_27_drift_graph_network_node_block_linear_wA
=assignvariableop_28_drift_graph_network_edge_block_linear_b_1A
=assignvariableop_29_drift_graph_network_edge_block_linear_w_1C
?assignvariableop_30_drift_graph_network_global_block_linear_b_1C
?assignvariableop_31_drift_graph_network_global_block_linear_w_1A
=assignvariableop_32_drift_graph_network_node_block_linear_b_1A
=assignvariableop_33_drift_graph_network_node_block_linear_w_1'
#assignvariableop_34_drift_conv1_d_b'
#assignvariableop_35_drift_conv1_d_w)
%assignvariableop_36_drift_conv1_d_b_1)
%assignvariableop_37_drift_conv1_d_w_1)
%assignvariableop_38_drift_conv1_d_b_2)
%assignvariableop_39_drift_conv1_d_w_2&
"assignvariableop_40_drift_linear_b&
"assignvariableop_41_drift_linear_w
identity_43ИвAssignVariableOpвAssignVariableOp_1вAssignVariableOp_10вAssignVariableOp_11вAssignVariableOp_12вAssignVariableOp_13вAssignVariableOp_14вAssignVariableOp_15вAssignVariableOp_16вAssignVariableOp_17вAssignVariableOp_18вAssignVariableOp_19вAssignVariableOp_2вAssignVariableOp_20вAssignVariableOp_21вAssignVariableOp_22вAssignVariableOp_23вAssignVariableOp_24вAssignVariableOp_25вAssignVariableOp_26вAssignVariableOp_27вAssignVariableOp_28вAssignVariableOp_29вAssignVariableOp_3вAssignVariableOp_30вAssignVariableOp_31вAssignVariableOp_32вAssignVariableOp_33вAssignVariableOp_34вAssignVariableOp_35вAssignVariableOp_36вAssignVariableOp_37вAssignVariableOp_38вAssignVariableOp_39вAssignVariableOp_4вAssignVariableOp_40вAssignVariableOp_41вAssignVariableOp_5вAssignVariableOp_6вAssignVariableOp_7вAssignVariableOp_8вAssignVariableOp_9 
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:+*
dtype0*Л
valueБB■+B*all_variables/0/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/1/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/2/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/3/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/4/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/5/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/6/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/7/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/8/.ATTRIBUTES/VARIABLE_VALUEB*all_variables/9/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/10/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/11/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/12/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/13/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/14/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/15/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/16/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/17/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/18/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/19/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/20/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/21/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/22/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/23/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/24/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/25/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/26/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/27/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/28/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/29/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/30/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/31/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/32/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/33/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/34/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/35/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/36/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/37/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/38/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/39/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/40/.ATTRIBUTES/VARIABLE_VALUEB+all_variables/41/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_namesф
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:+*
dtype0*i
value`B^+B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slicesЕ
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*┬
_output_shapesп
м:::::::::::::::::::::::::::::::::::::::::::*9
dtypes/
-2+2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identityи
AssignVariableOpAssignVariableOp)assignvariableop_drift_attention_linear_wIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1░
AssignVariableOp_1AssignVariableOp+assignvariableop_1_drift_attention_linear_bIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2▓
AssignVariableOp_2AssignVariableOp-assignvariableop_2_drift_attention_linear_w_1Identity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3▓
AssignVariableOp_3AssignVariableOp-assignvariableop_3_drift_attention_linear_b_1Identity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4▓
AssignVariableOp_4AssignVariableOp-assignvariableop_4_drift_attention_linear_w_2Identity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5▓
AssignVariableOp_5AssignVariableOp-assignvariableop_5_drift_attention_linear_b_2Identity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6▓
AssignVariableOp_6AssignVariableOp-assignvariableop_6_drift_attention_linear_w_3Identity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7▓
AssignVariableOp_7AssignVariableOp-assignvariableop_7_drift_attention_linear_b_3Identity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8▓
AssignVariableOp_8AssignVariableOp-assignvariableop_8_drift_attention_linear_w_4Identity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9▓
AssignVariableOp_9AssignVariableOp-assignvariableop_9_drift_attention_linear_b_4Identity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10╢
AssignVariableOp_10AssignVariableOp.assignvariableop_10_drift_attention_linear_w_5Identity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11╢
AssignVariableOp_11AssignVariableOp.assignvariableop_11_drift_attention_linear_w_6Identity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12╢
AssignVariableOp_12AssignVariableOp.assignvariableop_12_drift_attention_linear_b_5Identity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13╢
AssignVariableOp_13AssignVariableOp.assignvariableop_13_drift_attention_linear_w_7Identity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14╢
AssignVariableOp_14AssignVariableOp.assignvariableop_14_drift_attention_linear_b_6Identity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15╢
AssignVariableOp_15AssignVariableOp.assignvariableop_15_drift_attention_linear_w_8Identity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16╢
AssignVariableOp_16AssignVariableOp.assignvariableop_16_drift_attention_linear_b_7Identity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17╢
AssignVariableOp_17AssignVariableOp.assignvariableop_17_drift_attention_linear_w_9Identity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18╢
AssignVariableOp_18AssignVariableOp.assignvariableop_18_drift_attention_linear_b_8Identity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19╖
AssignVariableOp_19AssignVariableOp/assignvariableop_19_drift_attention_linear_w_10Identity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20╢
AssignVariableOp_20AssignVariableOp.assignvariableop_20_drift_attention_linear_b_9Identity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21╖
AssignVariableOp_21AssignVariableOp/assignvariableop_21_drift_attention_linear_w_11Identity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22├
AssignVariableOp_22AssignVariableOp;assignvariableop_22_drift_graph_network_edge_block_linear_bIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23├
AssignVariableOp_23AssignVariableOp;assignvariableop_23_drift_graph_network_edge_block_linear_wIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24┼
AssignVariableOp_24AssignVariableOp=assignvariableop_24_drift_graph_network_global_block_linear_bIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25┼
AssignVariableOp_25AssignVariableOp=assignvariableop_25_drift_graph_network_global_block_linear_wIdentity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26├
AssignVariableOp_26AssignVariableOp;assignvariableop_26_drift_graph_network_node_block_linear_bIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27├
AssignVariableOp_27AssignVariableOp;assignvariableop_27_drift_graph_network_node_block_linear_wIdentity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27n
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28┼
AssignVariableOp_28AssignVariableOp=assignvariableop_28_drift_graph_network_edge_block_linear_b_1Identity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29┼
AssignVariableOp_29AssignVariableOp=assignvariableop_29_drift_graph_network_edge_block_linear_w_1Identity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29n
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30╟
AssignVariableOp_30AssignVariableOp?assignvariableop_30_drift_graph_network_global_block_linear_b_1Identity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_30n
Identity_31IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:2
Identity_31╟
AssignVariableOp_31AssignVariableOp?assignvariableop_31_drift_graph_network_global_block_linear_w_1Identity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_31n
Identity_32IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:2
Identity_32┼
AssignVariableOp_32AssignVariableOp=assignvariableop_32_drift_graph_network_node_block_linear_b_1Identity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_32n
Identity_33IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:2
Identity_33┼
AssignVariableOp_33AssignVariableOp=assignvariableop_33_drift_graph_network_node_block_linear_w_1Identity_33:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_33n
Identity_34IdentityRestoreV2:tensors:34"/device:CPU:0*
T0*
_output_shapes
:2
Identity_34л
AssignVariableOp_34AssignVariableOp#assignvariableop_34_drift_conv1_d_bIdentity_34:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_34n
Identity_35IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:2
Identity_35л
AssignVariableOp_35AssignVariableOp#assignvariableop_35_drift_conv1_d_wIdentity_35:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_35n
Identity_36IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:2
Identity_36н
AssignVariableOp_36AssignVariableOp%assignvariableop_36_drift_conv1_d_b_1Identity_36:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_36n
Identity_37IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:2
Identity_37н
AssignVariableOp_37AssignVariableOp%assignvariableop_37_drift_conv1_d_w_1Identity_37:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_37n
Identity_38IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:2
Identity_38н
AssignVariableOp_38AssignVariableOp%assignvariableop_38_drift_conv1_d_b_2Identity_38:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_38n
Identity_39IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:2
Identity_39н
AssignVariableOp_39AssignVariableOp%assignvariableop_39_drift_conv1_d_w_2Identity_39:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_39n
Identity_40IdentityRestoreV2:tensors:40"/device:CPU:0*
T0*
_output_shapes
:2
Identity_40к
AssignVariableOp_40AssignVariableOp"assignvariableop_40_drift_linear_bIdentity_40:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_40n
Identity_41IdentityRestoreV2:tensors:41"/device:CPU:0*
T0*
_output_shapes
:2
Identity_41к
AssignVariableOp_41AssignVariableOp"assignvariableop_41_drift_linear_wIdentity_41:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_419
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp·
Identity_42Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_42э
Identity_43IdentityIdentity_42:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*
T0*
_output_shapes
: 2
Identity_43"#
identity_43Identity_43:output:0*┐
_input_shapesн
к: ::::::::::::::::::::::::::::::::::::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
Н
д
 drift_dropout_cond_false_11265739
5drift_dropout_cond_dropout_mul_drift_attention_matmul
drift_dropout_cond_identity!
drift_dropout_cond_identity_1
ИЙ
 drift/dropout/cond/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *в╝Ж?2"
 drift/dropout/cond/dropout/Constр
drift/dropout/cond/dropout/MulMul5drift_dropout_cond_dropout_mul_drift_attention_matmul)drift/dropout/cond/dropout/Const:output:0*
T0*,
_output_shapes
:         @А2 
drift/dropout/cond/dropout/Mulй
 drift/dropout/cond/dropout/ShapeShape5drift_dropout_cond_dropout_mul_drift_attention_matmul*
T0*
_output_shapes
:2"
 drift/dropout/cond/dropout/ShapeЄ
7drift/dropout/cond/dropout/random_uniform/RandomUniformRandomUniform)drift/dropout/cond/dropout/Shape:output:0*
T0*,
_output_shapes
:         @А*
dtype029
7drift/dropout/cond/dropout/random_uniform/RandomUniformЫ
)drift/dropout/cond/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L=2+
)drift/dropout/cond/dropout/GreaterEqual/yП
'drift/dropout/cond/dropout/GreaterEqualGreaterEqual@drift/dropout/cond/dropout/random_uniform/RandomUniform:output:02drift/dropout/cond/dropout/GreaterEqual/y:output:0*
T0*,
_output_shapes
:         @А2)
'drift/dropout/cond/dropout/GreaterEqual╜
drift/dropout/cond/dropout/CastCast+drift/dropout/cond/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*,
_output_shapes
:         @А2!
drift/dropout/cond/dropout/Cast╦
 drift/dropout/cond/dropout/Mul_1Mul"drift/dropout/cond/dropout/Mul:z:0#drift/dropout/cond/dropout/Cast:y:0*
T0*,
_output_shapes
:         @А2"
 drift/dropout/cond/dropout/Mul_1v
drift/dropout/cond/ConstConst*
_output_shapes
: *
dtype0
*
value	B
 Z2
drift/dropout/cond/Constz
drift/dropout/cond/Const_1Const*
_output_shapes
: *
dtype0
*
value	B
 Z2
drift/dropout/cond/Const_1г
drift/dropout/cond/IdentityIdentity$drift/dropout/cond/dropout/Mul_1:z:0*
T0*,
_output_shapes
:         @А2
drift/dropout/cond/Identityz
drift/dropout/cond/Const_2Const*
_output_shapes
: *
dtype0
*
value	B
 Z2
drift/dropout/cond/Const_2Р
drift/dropout/cond/Identity_1Identity#drift/dropout/cond/Const_2:output:0*
T0
*
_output_shapes
: 2
drift/dropout/cond/Identity_1"C
drift_dropout_cond_identity$drift/dropout/cond/Identity:output:0"G
drift_dropout_cond_identity_1&drift/dropout/cond/Identity_1:output:0*+
_input_shapes
:         @А:2 .
,
_output_shapes
:         @А
╠
Э
drift_dropout_cond_true_11265726
2drift_dropout_cond_identity_drift_attention_matmul
drift_dropout_cond_identity!
drift_dropout_cond_identity_1
▒
drift/dropout/cond/IdentityIdentity2drift_dropout_cond_identity_drift_attention_matmul*
T0*,
_output_shapes
:         @А2
drift/dropout/cond/Identityv
drift/dropout/cond/ConstConst*
_output_shapes
: *
dtype0
*
value	B
 Z2
drift/dropout/cond/ConstО
drift/dropout/cond/Identity_1Identity!drift/dropout/cond/Const:output:0*
T0
*
_output_shapes
: 2
drift/dropout/cond/Identity_1"C
drift_dropout_cond_identity$drift/dropout/cond/Identity:output:0"G
drift_dropout_cond_identity_1&drift/dropout/cond/Identity_1:output:0*+
_input_shapes
:         @А:2 .
,
_output_shapes
:         @А
э
А
%__inference_signature_wrapper_1126708
comp

comp_1

comp_2

comp_3

comp_4

comp_5

comp_6
is_training

	mask_comp
	mask_prot
prot
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16

unknown_17

unknown_18

unknown_19

unknown_20

unknown_21

unknown_22

unknown_23

unknown_24

unknown_25

unknown_26

unknown_27

unknown_28

unknown_29

unknown_30

unknown_31

unknown_32

unknown_33

unknown_34

unknown_35

unknown_36

unknown_37

unknown_38

unknown_39

unknown_40
identity

identity_1ИвStatefulPartitionedCallэ
StatefulPartitionedCallStatefulPartitionedCallcompcomp_1comp_2comp_3comp_4comp_5comp_6prot	mask_comp	mask_protis_trainingunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30
unknown_31
unknown_32
unknown_33
unknown_34
unknown_35
unknown_36
unknown_37
unknown_38
unknown_39
unknown_40*@
Tin9
725
*
Tout
2*
_collective_manager_ids
 *;
_output_shapes)
':         :         @А*L
_read_only_resource_inputs.
,* !"#$%&'()*+,-./01234*0
config_proto 

CPU

GPU2*0J 8В *&
f!R
__inference_inference_11266052
StatefulPartitionedCallК
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*#
_output_shapes
:         2

IdentityЧ

Identity_1Identity StatefulPartitionedCall:output:1^StatefulPartitionedCall*
T0*,
_output_shapes
:         @А2

Identity_1"
identityIdentity:output:0"!

identity_1Identity_1:output:0*є
_input_shapesс
▐:         R:         :         :         :          :         :         ::         @:         А:         А::::::::::::::::::::::::::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:M I
'
_output_shapes
:         R

_user_specified_namecomp:OK
'
_output_shapes
:         
 
_user_specified_namecomp_1:KG
#
_output_shapes
:         
 
_user_specified_namecomp_2:KG
#
_output_shapes
:         
 
_user_specified_namecomp_3:MI
%
_output_shapes
:          
 
_user_specified_namecomp_4:KG
#
_output_shapes
:         
 
_user_specified_namecomp_5:KG
#
_output_shapes
:         
 
_user_specified_namecomp_6:EA

_output_shapes
:
%
_user_specified_nameis_training:RN
'
_output_shapes
:         @
#
_user_specified_name	mask_comp:S	O
(
_output_shapes
:         А
#
_user_specified_name	mask_prot:R
N
,
_output_shapes
:         А

_user_specified_nameprot"╕L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*й
serving_defaultХ
5
comp-
serving_default_comp:0         R
9
comp_1/
serving_default_comp_1:0         
5
comp_2+
serving_default_comp_2:0         
5
comp_3+
serving_default_comp_3:0         
7
comp_4-
serving_default_comp_4:0          
5
comp_5+
serving_default_comp_5:0         
5
comp_6+
serving_default_comp_6:0         
4
is_training%
serving_default_is_training:0

?
	mask_comp2
serving_default_mask_comp:0         @
@
	mask_prot3
serving_default_mask_prot:0         А
:
prot2
serving_default_prot:0         А8
output_0,
StatefulPartitionedCall:0         A
output_15
StatefulPartitionedCall:1         @Аtensorflow/serving/predict:Г#
P
all_variables

signatures
-	inference"
_generic_user_object
ц
0
1
2
3
4
5
	6

7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
 29
!30
"31
#32
$33
%34
&35
'36
(37
)38
*39
+40
,41"
trackable_list_wrapper
,
.serving_default"
signature_map
,:*
А└2drift/attention/linear/w
':%А2drift/attention/linear/b
+:)	 А2drift/attention/linear/w
':%А2drift/attention/linear/b
,:*
АА2drift/attention/linear/w
':%А2drift/attention/linear/b
,:*
АА2drift/attention/linear/w
':%А2drift/attention/linear/b
,:*
АА2drift/attention/linear/w
':%А2drift/attention/linear/b
,:*
АА2drift/attention/linear/w
,:*
А└2drift/attention/linear/w
':%А2drift/attention/linear/b
,:*
АА2drift/attention/linear/w
':%А2drift/attention/linear/b
,:*
АА2drift/attention/linear/w
':%А2drift/attention/linear/b
,:*
АА2drift/attention/linear/w
':%А2drift/attention/linear/b
,:*
АА2drift/attention/linear/w
':%А2drift/attention/linear/b
,:*
АА2drift/attention/linear/w
5:3 2'drift/graph_network/edge_block/linear/b
::8	к 2'drift/graph_network/edge_block/linear/w
7:52)drift/graph_network/global_block/linear/b
;:9@2)drift/graph_network/global_block/linear/w
5:3 2'drift/graph_network/node_block/linear/b
9:7r 2'drift/graph_network/node_block/linear/w
5:3 2'drift/graph_network/edge_block/linear/b
9:7a 2'drift/graph_network/edge_block/linear/w
7:52)drift/graph_network/global_block/linear/b
;:9A2)drift/graph_network/global_block/linear/w
5:3 2'drift/graph_network/node_block/linear/b
9:7A 2'drift/graph_network/node_block/linear/w
: 2drift/conv1_d/b
%:#  2drift/conv1_d/w
: 2drift/conv1_d/b
%:#  2drift/conv1_d/w
: 2drift/conv1_d/b
%:#  2drift/conv1_d/w
: 2drift/linear/b
 : 2drift/linear/w
╧2╠
__inference_inference_1126605к
└▓╝
FullArgSpecD
args<Ъ9
jcomp
jprot
j	mask_comp
j	mask_prot
jis_training
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *фвр
■▓·
GraphsTuple!
nodesК         R!
edgesК         !
	receiversК         
sendersК         !
globalsК          
n_nodeК         
n_edgeК         
К         А
К         @
К         А
	К

ЙBЖ
%__inference_signature_wrapper_1126708compcomp_1comp_2comp_3comp_4comp_5comp_6is_training	mask_comp	mask_protprotИ
__inference_inference_1126605ц* $#"!,+&%('*)	
ўвє
ывч
▄▓╪
GraphsTuple-
nodes$К!

comp/nodes         R-
edges$К!

comp/edges         1
	receivers$К!
comp/receivers         -
senders"К
comp/senders         /
globals$К!
comp/globals          +
n_node!К
comp/n_node         +
n_edge!К
comp/n_edge         
#К 
prot         А
#К 
	mask_comp         @
$К!
	mask_prot         А
К
is_training

к ">в;
К
0         
 К
1         @Аж
%__inference_signature_wrapper_1126708№* $#"!,+&%('*)	
чву
в 
█к╫
&
compК
comp         R
*
comp_1 К
comp_1         
&
comp_2К
comp_2         
&
comp_3К
comp_3         
(
comp_4К
comp_4          
&
comp_5К
comp_5         
&
comp_6К
comp_6         
%
is_trainingК
is_training

0
	mask_comp#К 
	mask_comp         @
1
	mask_prot$К!
	mask_prot         А
+
prot#К 
prot         А"dкa
*
output_0К
output_0         
3
output_1'К$
output_1         @А